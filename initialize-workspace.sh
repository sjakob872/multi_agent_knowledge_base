#!/bin/bash

sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update
sudo apt install ros-melodic-desktop-full

sudo apt install net-tools flex autoconf automake libtool cmake bison re2c build-essential g++ gitk uuid-dev python-catkin-tools libnl-utils lua5.3 doxygen libyaml-cpp0.5v5 libyaml-cpp-dev curl libjsoncpp-dev libcurl4-openssl-dev 

git clone https://github.com/potassco/clingo.git
cd clingo/
mkdir build
git submodule update --init --recursive
cd build
cmake ..
make
sudo make install

cd ../..

mkdir src
catkin init

cd src

git clone https://github.com/dasys-lab/aspsuite.git
git clone https://github.com/dasys-lab/essentials.git
git clone https://github.com/StefanJakob/cantcoap.git
git clone https://github.com/StefanJakob/jsoncpp.git
git clone https://github.com/dasys-lab/alica.git
git clone git@bitbucket.org:sjakob872/multi_agent_knowledge_base.git

cd aspsuite
git checkout kr_dev
cd ..

cd essentials
git checkout ttb_dev
cd ..

cd alica
git checkout newPD
cd ..

echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
echo "source ~/workspace/devel/setup.bash" >> ~/.bashrc

source ~/.bashrc

cd ..

#catkin build
