#include "knowledge_registry/node/PingManager.h"

#include "knowledge_registry/network/NetworkManager.h"
#include "knowledge_registry/node/ASPParser.h"
#include "knowledge_registry/node/Contact.h"
#include "knowledge_registry/node/Node.h"
#include "knowledge_registry/node/event/OutEvent.h"
#include "knowledge_registry/node/event/InEvent.h"
#include "knowledge_registry/node/SolverManager.h"

#include <reasoner/asp/Solver.h>

namespace knowledge_registry
{
namespace node
{
const int PingManager::pingLimit = 3;
PingManager::PingManager(knowledge_registry::node::Node* node)
        : node(node)
{
    this->startPingThread = new std::thread(&PingManager::startPing, this);
    this->sleepFlag = false;
}

PingManager::~PingManager()
{
    this->sleepFlag = true;
    this->sleepCV.notify_all();
    this->pingCV.notify_all();
    this->startPingThread->join();
    delete this->startPingThread;
}

void PingManager::notify()
{
    this->pingCV.notify_all();
}

void PingManager::startPing()
{
    while (this->node->isStay()) {
        std::unique_lock<std::mutex> lk(this->pingMutex);
        this->pingCV.wait(lk);
        lk.unlock();

        while (this->node->getRegistry()) {
            if (this->node->getNodeType() == common::NodeType::REGISTRY) {
                std::thread pingThread(&PingManager::pingLeafs, this);
                pingThread.join();
            } else if (this->node->getNodeType() == common::NodeType::LEAF) {
                std::thread getPingThread(&PingManager::getPing, this);
                getPingThread.join();
            } else {
                break;
            }
        }
    }
}

void PingManager::pingLeafs()
{
    while (true) {
        std::unique_lock<std::mutex> lock(this->sleepMutex);
        this->sleepCV.wait_for(lock, std::chrono::seconds(10), [this]() { return this->sleepFlag; });
        if (!this->node->isStay()) {
            return;
        }
        std::vector<Contact*> nodes = ASPParser::parse(this->node, this->node->getLastASPNetworkModel(), common::ParserType::NODE);
        std::cout << "PingManager: pinging " << nodes.size() << " leaf(s)." << std::endl;
        std::cout << "PingManager: Current Model: " << this->node->getLastASPNetworkModel() << std::endl;
        for (auto& contact : nodes) {
            if (contact->getUuid() != this->node->getRegistry()->getUuid()) {
                // Put ping event to the networkManager out put queue.
                std::cout << "PingManager: pinging " << contact->getUuid() << std::endl;
                if (pingLeaf(contact)) {
                    this->node->getNetworkManager()->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
                            OutEvent(common::MessageType::TCP, common::MessageRole::PING, contact->getIP(), contact->getPort(), contact->getUuid())));
                } else {
                    std::cout << "\033[1;31mPingManager: Number of pings for " << contact->toString() << " exceeded the limit "
                              << "\033[0m" << std::endl;
                    std::string externalString = ASPParser::getExternal(contact, false);
                    reasoner::asp::Solver* networkSolver = this->node->getSolverManager()->getNetworkSolverPair()->second;
                    networkSolver->assignExternal(networkSolver->parseValue(externalString), Clingo::TruthValue::True);
                    networkSolver->solve();

                    std::stringstream ss;
                    for (size_t i = 0; i < networkSolver->getCurrentModels().at(0).size(); i++) {
                        ss << networkSolver->getCurrentModels().at(0).at(i) << ".";
                    }
                    this->node->setLastASPNetworkModel(ss.str());
                    std::cout << "PingManager: Answer Set after no ping received: " << ss.str() << std::endl;
                }
            }
        }
    }
}

// Receive ping from Root
void PingManager::getPing()
{
    while (this->node->registryAlive) {
        this->node->setRegistryAlive(false);
        std::unique_lock<std::mutex> lock(this->sleepMutex);
        this->sleepCV.wait_for(lock, std::chrono::seconds(15), [this]() { return this->sleepFlag; });
    }
    if (!this->node->isStay()) {
        return;
    }
    std::cout << "\033[1;31mPingManager: Registry is dead " << this->node->getRegistry()->getUuid() << "\033[0m" << std::endl;
    reasoner::asp::Solver* networkSolver = this->node->getSolverManager()->getNetworkSolverPair()->second;
    networkSolver->assignExternal(networkSolver->parseValue("-registry(uuid(\"" + this->node->getRegistry()->getUuid() + "\"))"), Clingo::TruthValue::True);
    networkSolver->assignExternal(networkSolver->parseValue("-leaf(uuid(\"" + this->node->getUuid() + "\"))"), Clingo::TruthValue::True);
    networkSolver->solve();

    std::stringstream ss;
    for (size_t i = 0; i < networkSolver->getCurrentModels().at(0).size(); i++) {
        ss << networkSolver->getCurrentModels().at(0).at(i) << ".";
    }
    this->node->setLastASPNetworkModel(ss.str());
    std::cout << "PingManager: Answer Set after no ping received: " << ss.str() << std::endl;

    this->node->broadcastMessageList.clear();
    this->node->getNetworkManager()->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
            OutEvent(common::MessageType::BROADCAST, common::MessageRole::ELECTION, this->node->getIP(),
                    this->node->getPort(), this->node->getUuid(), this->node->getRegistry()->getUuid())));
    this->node->unsetRegistry();
}

bool PingManager::pingLeaf(Contact* contact)
{
    if (this->pingedLeafs.find(contact) == this->pingedLeafs.end()) {
        this->pingedLeafs.emplace(contact, 1);
        return true;
    }
    int pings = this->pingedLeafs.at(contact) + 1;
    if (pings <= PingManager::pingLimit) {
        this->pingedLeafs.at(contact) = pings;
        return true;
    } else {
        this->pingedLeafs.erase(contact);
        return false;
    }
}

void PingManager::pingReceivedFromLeaf(Contact* contact)
{
    if (this->pingedLeafs.find(contact) == this->pingedLeafs.end()) {
        std::cout << "\033[1;31mPingManager: trying to reduce number of open pings for not contacted leaf!\033[0m" << std::endl;
        return;
    }
    int pings = this->pingedLeafs.at(contact);
    this->pingedLeafs.at(contact) = (pings > 0 ? (pings - 1) : 0);
}

} // namespace node
} // namespace knowledge_registry
