#include "knowledge_registry/node/NodeUtils.h"

#include "knowledge_registry/common/InformationElement.h"
#include "knowledge_registry/node/ASPParser.h"
#include "knowledge_registry/node/Node.h"
#include "knowledge_registry/node/SolverManager.h"
#include "knowledge_registry/node/event/InEvent.h"

#include <reasoner/asp/AnnotatedValVec.h>
#include <reasoner/asp/Query.h>
#include <reasoner/asp/Solver.h>
#include <reasoner/asp/Variable.h>

#include <string>

namespace knowledge_registry
{
namespace node
{
NodeUtils::NodeUtils(Node* node)
{
    this->node = node;
    this->counter = 0;
}

std::string NodeUtils::getNetworkSolution(
        const std::shared_ptr<const common::InformationElement<node::InEvent>> event, std::pair<common::SolverType, reasoner::asp::Solver*>* solverPair)
{
    bool isRegistryMessage = event->getInformation().getUuid() == this->node->getRegistry()->getUuid();

    auto ps = "ps" + std::to_string(this->counter++);
    if (this->getFittingPredicates(ASPParser::getASPRepresentation(this->node, event, isRegistryMessage), solverPair).size() == 0) {
        solverPair->second->add(ps.c_str(), {}, ASPParser::generateASPContact(event, isRegistryMessage).c_str());
        solverPair->second->ground({{ps.c_str(), {}}}, nullptr);
    }
    std::string solution = "";
    if (solverPair->second->solve()) {
        solution = setLasModel(solverPair);
    }
    return solution;
}

std::string NodeUtils::getSolution(
        const std::shared_ptr<const common::InformationElement<node::InEvent>> event, std::pair<common::SolverType, reasoner::asp::Solver*>* solverPair)
{
    solverPair->second->add("", {}, event->getInformation().getMessage().c_str());
    solverPair->second->ground({{"", {}}}, nullptr);
    std::string solution = "";
    if (solverPair->second->solve()) {
        solution = setLasModel(solverPair);
    }
    return solution;
}

std::string NodeUtils::setLasModel(std::pair<common::SolverType, reasoner::asp::Solver*>* solverPair) const
{
    std::string solution;
    auto models = solverPair->second->getCurrentModels();
    for (unsigned int a = 0; a < models.size(); a++) {
        //  get the first solution
        for (unsigned int i = 0; i < models.at(0).size(); i++) {
            solution += models.at(0).at(i).to_string() + ".";
        }
    }
    if (solverPair->first == common::SolverType::NETWORK) {
        this->node->setLastASPNetworkModel(solution);
    } else if (solverPair->first == common::SolverType::KNOWLEDGE_BASE) {
        this->node->setLastASPKnowledgeBaseModel(solution);
    } else {
        std::cout << "\033[1;31mNodeUtils: solver with type: " << common::solverTypes[solverPair->first] << " not supported!\033[0m" << std::endl;
    }
    return solution;
}

std::vector<Contact*> NodeUtils::getLeafs()
{
    std::vector<std::string> leafs = getFittingPredicates("leaf(" + reasoner::asp::Solver::WILDCARD_STRING + ", " + reasoner::asp::Solver::WILDCARD_STRING +
                                                                  ", " + reasoner::asp::Solver::WILDCARD_STRING + ")",
            this->node->getSolverManager()->getNetworkSolverPair());
    std::vector<Contact*> ret;
    for (std::string& leaf : leafs) {
        std::vector<Contact*> contacts = ASPParser::parse(this->node, leaf, common::ParserType::NODE);
        ret.insert(ret.end(), contacts.begin(), contacts.end());
    }
    return ret;
}

std::vector<std::string> NodeUtils::getFittingPredicates(std::string aspQuery, std::pair<common::SolverType, reasoner::asp::Solver*>* solverPair)
{
    std::vector<std::string> model;
    auto term = new reasoner::asp::Term();
    term->setType(reasoner::asp::QueryType::Filter);
    int queryId = solverPair->second->getQueryCounter();
    term->setId(queryId);
    term->setQueryId(queryId);
    term->setQueryRule(aspQuery);

    std::vector<reasoner::asp::Variable*> vars;
    vars.push_back(new reasoner::asp::Variable());
    std::vector<reasoner::asp::Term*> terms;
    terms.push_back(term);
    std::vector<reasoner::asp::AnnotatedValVec*> result;
    solverPair->second->getSolution(vars, terms, result);
    if (!result.empty()) {
        if (!result.at(0)->factQueryValues.empty()) {
            for (size_t i = 0; i < result.size(); i++) {
                for (size_t j = 0; j < result.at(i)->factQueryValues.size(); j++) {
                    for (size_t k = 0; k < result.at(i)->factQueryValues.at(j).size(); k++) {
                        model.push_back(result.at(i)->factQueryValues.at(j).at(k));
                    }
                }
            }
            setLasModel(solverPair);
        }
    } else {
        std::cout << "\033[1;31mNodeUtils: Result for query " << aspQuery << "is empty!\033[0m" << std::endl;
    }
    return model;
}

} // namespace node
} // namespace knowledge_registry