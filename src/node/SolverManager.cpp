#include "knowledge_registry/node/SolverManager.h"
#include "knowledge_registry/node/Node.h"

#include <reasoner/asp/Solver.h>

namespace knowledge_registry
{
namespace node
{
SolverManager::SolverManager(Node* node)
        : node(node)
{
    this->knowledgeBaseSolverPair =
            new std::pair<common::SolverType, reasoner::asp::Solver*>(common::SolverType::KNOWLEDGE_BASE, new reasoner::asp::Solver({}));
    this->networkSolverPair = new std::pair<common::SolverType, reasoner::asp::Solver*>(common::SolverType::NETWORK, new reasoner::asp::Solver({}));
    this->knowledgeBaseDebugSolverPair =
            new std::pair<common::SolverType, reasoner::asp::Solver*>(common::SolverType::KNOWLEDGE_BASE_DEBUG, new reasoner::asp::Solver({}));
    initSolvers();
}

SolverManager::~SolverManager()
{
    delete this->knowledgeBaseSolverPair;
    delete this->networkSolverPair;
    delete this->knowledgeBaseDebugSolverPair;
}

void SolverManager::initSolvers()
{
    std::stringstream ss;
    std::string iPAddress = this->node->getIP();
    std::replace(iPAddress.begin(), iPAddress.end(), '.', ',');
    ss << "ip(" << iPAddress << ").\n";
    ss << "port(" << this->node->getPort() << ").\n";
    ss << "uuid(\"" << this->node->getUuid() << "\").";
    this->networkSolverPair->second->add("", {}, ss.str().c_str());
    this->networkSolverPair->second->ground({{"", {}}}, nullptr);
}

std::pair<common::SolverType, reasoner::asp::Solver*>* SolverManager::getNetworkSolverPair() const
{
    return this->networkSolverPair;
}

std::pair<common::SolverType, reasoner::asp::Solver*>* SolverManager::getKnowledgeBaseSolverPair() const
{
    return this->knowledgeBaseSolverPair;
}

std::pair<common::SolverType, reasoner::asp::Solver*>* SolverManager::getKnowledgeBaseDebugSolverPair() const
{
    return this->knowledgeBaseDebugSolverPair;
}
} // namespace node
} // namespace knowledge_registry
