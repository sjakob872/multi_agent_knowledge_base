#include "knowledge_registry/node/ASPParser.h"

#include "knowledge_registry/common/InformationElement.h"
#include "knowledge_registry/node/Contact.h"
#include "knowledge_registry/node/ContactManager.h"
#include "knowledge_registry/node/Node.h"
#include "knowledge_registry/node/event/InEvent.h"

#include <regex>

namespace knowledge_registry
{
namespace node
{
std::vector<Contact*> ASPParser::parse(Node* node, std::string aspString, common::ParserType parserType)
{
    std::string regularExp;
    switch (parserType) {
    case (common::ParserType::IP_PORT):
        regularExp = "(ip\\([0-9,\\,]+\\)\\.port\\([0-9]+\\))";
        break;
    case (common::ParserType::NODE):
        regularExp = "(leaf\\(uuid\\(\"[a-zA-Z0-9-]+\"\\)\\,ip\\([0-9,\\,]+\\),port\\([0-9]+\\)\\))";
        break;
    case (common::ParserType::REG):
        regularExp = "(registry\\(uuid\\(\"[a-zA-Z0-9-]+\"\\)\\,ip\\([0-9,\\,]+\\),port\\([0-9]+\\)\\))";
        break;
    }
    return extractContacts(node, aspString, regularExp);
}

std::vector<Contact*> ASPParser::extractContacts(Node* node, std::string aspString, std::string regularExp)
{
    std::vector<Contact*> addresses;
    std::regex re(regularExp);
    std::smatch match;
    std::smatch ipMatch;
    std::smatch portMatch;
    std::smatch uuidMatch;
    std::smatch topicMatch;
    std::vector<std::string> myIpsPort;
    std::regex ip("(ip\\([0-9,\\,]+\\))");
    std::regex port("(port(\\([0-9]+\\)))");
    // std::regex uuid("(uuid(\\(\"[\\w\\-\\.]+[\\-\\.][\\w\\-\\.]+\"\\)))");
    std::regex uuid("(uuid\\(\"[a-zA-Z0-9-]+\"\\))");
    std::regex topic("(topic(\\(\\w+\\)))");
    std::regex portValue("([0-9]+)");
    std::regex ipValue("([0-9,]+)");
    std::regex uuidValue("(\"[a-zA-Z0-9-]+\")");
    std::regex topicValue("(\\w+)");

    while (regex_search(aspString, ipMatch, re)) {
        for (auto x : ipMatch) {
            if ((find(myIpsPort.begin(), myIpsPort.end(), x)) == myIpsPort.end())
                myIpsPort.push_back(x);
        }
        aspString = ipMatch.suffix().str();
    }
    std::string tmpIp;
    std::string tmpPort;
    std::string tmpUUID;
    std::string tmpTopic;

    for (auto it : myIpsPort) {
        // IP
        if (regex_search(it, ipMatch, ip)) {
            for (auto& elt : ipMatch) {
                std::string A = elt.str();
                std::smatch mIP1;
                if (regex_search(A, mIP1, ipValue)) {
                    for (auto& elt1 : mIP1)
                        tmpIp = elt1.str();
                    A = mIP1.suffix().str();
                }
            }
            replace(tmpIp.begin(), tmpIp.end(), ',', '.');
            // it = ipMatch.suffix().str();
        }
        // PORT
        if (regex_search(it, portMatch, port)) {
            std::string matchString = portMatch.str();
            std::smatch mPort1;

            if (regex_search(matchString, mPort1, ipValue)) {
                tmpPort = mPort1.str();
                matchString = mPort1.suffix().str();
            }
            // it = portMatch.suffix().str();
        }
        // UUID
        if (regex_search(it, uuidMatch, uuid)) {
            std::string b = uuidMatch.str();
            std::smatch uuidM;
            if (regex_search(b, uuidM, uuidValue)) {
                tmpUUID = uuidM.str();
                tmpUUID.erase(0, 1);
                tmpUUID.erase(tmpUUID.length() - 1, 1);
                b = uuidM.suffix().str();
            }
            // it = uuidM.suffix().str();
        }
        // TOPIC
        if (regex_search(it, topicMatch, topic)) {
            std::string B = topicMatch.str();
            std::smatch mtopic1;

            if (regex_search(B, mtopic1, topicValue)) {
                tmpTopic = mtopic1.str();
                B = mtopic1.suffix().str();
            }
            // it = topicMatch.suffix().str();
        }
        addresses.push_back(node->getContactManager()->getContact(tmpIp, tmpPort, tmpUUID, tmpTopic));
    }
    return addresses;
}

// Build the Asp predicate for registration
// registry(uuid(..), ip(...), port(...)). topic(uuid(...), unrefined).
// leaf(uuid(..), ip(...), port(...)). topic(uuid(...), unrefined).
std::string ASPParser::generateASPContact(const std::shared_ptr<const common::InformationElement<node::InEvent>> event, bool registry)
{
    std::string iPAddress = event->getInformation().getIP();
    std::stringstream addressToAsp;
    std::replace(iPAddress.begin(), iPAddress.end(), '.', ',');
    if (registry) {
        addressToAsp << "#external -registry(uuid(\"" << event->getInformation().getUuid() << "\")). \n"
                     << "registry(uuid(\"" << event->getInformation().getUuid() << "\"),ip(" << iPAddress << "),"
                     << "port(" << event->getInformation().getPort() << ")) :- not -registry(uuid(\"" << event->getInformation().getUuid() << "\")).";
    } else {

        addressToAsp << "#external -leaf(uuid(\"" << event->getInformation().getUuid() << "\")). \n"
                     << "leaf(uuid(\"" << event->getInformation().getUuid() << "\"),ip(" << iPAddress << "),"
                     << "port(" << event->getInformation().getPort() << ")) :- not -leaf(uuid(\"" << event->getInformation().getUuid() << "\")). \n"
                     << "topic(uuid(\"" << event->getInformation().getUuid() << "\"),undefined) :- not -leaf(uuid(\"" << event->getInformation().getUuid()
                     << "\")).";
    }
    return addressToAsp.str();
}

std::string ASPParser::getExternal(knowledge_registry::node::Contact* contact, bool registry)
{
    if (registry) {
        return "-registry(uuid(\"" + contact->getUuid() + "\"))";
    } else {
        return "-leaf(uuid(\"" + contact->getUuid() + "\"))";
    }
}

std::string ASPParser::getASPRepresentation(Node* node, const std::shared_ptr<const common::InformationElement<node::InEvent>> event, bool root)
{
    std::string iPAddress = event->getInformation().getIP();
    std::stringstream addressToAsp;
    std::replace(iPAddress.begin(), iPAddress.end(), '.', ',');
    if (root) {
        addressToAsp << "registry(uuid(\"" << node->getRegistry()->getUuid() << "\"),ip(" << iPAddress << "),"
                     << "port(" << event->getInformation().getPort() << "))";
    } else {
        addressToAsp << "leaf(uuid(\"" << event->getInformation().getUuid() << "\"),ip(" << iPAddress << "),"
                     << "port(" << event->getInformation().getPort() << "))";
    }
    return addressToAsp.str();
}
} // namespace node
} // namespace knowledge_registry