#include "knowledge_registry/node/event/OutEvent.h"

namespace knowledge_registry
{
namespace node
{
OutEvent::OutEvent(common::MessageType type, common::MessageRole role, std::string ip, std::string port, std::string uuid, std::string message)
        : Event(type, role, ip, port, uuid, message)
{
}
} // namespace node
} // namespace knowledge_registry