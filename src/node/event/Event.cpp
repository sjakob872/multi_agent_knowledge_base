#include "knowledge_registry/node/event/Event.h"

#include <sstream>

namespace knowledge_registry
{
namespace node
{
Event::Event(common::MessageType type, common::MessageRole role, std::string ip, std::string port, std::string uuid, std::string message)
        : type(type)
        , role(role)
        , ip(ip)
        , port(port)
        , message(message)
        , uuid(uuid)
{
}

void Event::Serialize(Json::Value& root)
{
    root["ip"] = Event::getIP();
    root["port"] = Event::getPort();
    root["role"] = Event::getRole();
    root["uuid"] = Event::getUuid();
    root["type"] = Event::getType();
    root["message"] = Event::getMessage();
}

void Event::Deserialize(Json::Value& root)
{
    Event::ip = (root.get("ip", "DefaultIP").asString());
    Event::port = (root.get("port", "DefaultPort").asString());
    Event::role = static_cast<common::MessageRole>(root.get("role", "DefaultRole").asInt());
    Event::uuid = root.get("uuid", "DefaultUUID").asString();
    Event::type = static_cast<common::MessageType>(root.get("type", "DefaultMessageType").asInt());
    Event::message = root.get("message", "DefaultMessage").asString();
}

std::string Event::toString()
{
    std::stringstream ss;
    ss << common::messageTypes[this->type] << " : ";
    ss << common::messageRoles[this->role] << " : ";
    ss << this->ip << " : ";
    ss << this->port << " : ";
    ss << this->uuid << " : ";
    ss << this->message;
    return ss.str();
}

common::MessageType Event::getType() const
{
    return this->type;
}

common::MessageRole Event::getRole() const
{
    return this->role;
}

std::string Event::getIP() const
{
    return this->ip;
}

std::string Event::getPort() const
{
    return this->port;
}

std::string Event::getUuid() const
{
    return this->uuid;
}

std::string Event::getMessage() const
{
    return this->message;
}
} // namespace node
} // namespace knowledge_registry