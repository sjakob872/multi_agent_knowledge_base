#include "knowledge_registry/node/Contact.h"

#include <sstream>

namespace knowledge_registry
{
namespace node
{
Contact::Contact(std::string ip, std::string port, std::string uuid, std::string topic)
        : ip(ip)
        , port(port)
        , uuid(uuid)
        , topic(topic)
{
}

std::string Contact::toString()
{
    std::stringstream ss;
    ss << this->ip << " : ";
    ss << this->port << " : ";
    ss << this->uuid << " : ";
    ss << this->topic;
    return ss.str();
}

const std::string& Contact::getIP()
{
    return this->ip;
}

const std::string& Contact::getPort()
{
    return this->port;
}

const std::string& Contact::getUuid()
{
    return this->uuid;
}

const std::string& Contact::getTopic()
{
    return this->topic;
}
} // namespace node
} // namespace knowledge_registry