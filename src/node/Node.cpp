#include "knowledge_registry/node/Node.h"

#include "knowledge_registry/common/InformationElement.h"
#include "knowledge_registry/common/JsonSerializer.h"
#include "knowledge_registry/common/KREnums.h"
#include "knowledge_registry/network/NetworkManager.h"
#include "knowledge_registry/node/Contact.h"
#include "knowledge_registry/node/ContactManager.h"
#include "knowledge_registry/node/NodeUtils.h"
#include "knowledge_registry/node/PingManager.h"
#include "knowledge_registry/node/SolverManager.h"
#include "knowledge_registry/node/event/InEvent.h"
#include "knowledge_registry/node/event/OutEvent.h"

#include <arpa/inet.h>
#include <cantcoap/nethelper.h>
#include <netdb.h>
#include <random>
#include <sstream>
#include <cstring>
#include <uuid/uuid.h>

namespace knowledge_registry
{
namespace node
{
bool Node::running = false;
const std::string Node::UNDEFINED_TOPIC = "UNDEFINED";

void Node::init()
{
    this->getOwnNetworkConfiguration();
    this->port = this->getFreePort();
    this->setUuid();
    std::cout << "Node: Uuid: " << this->uuid << "\n"
              << "Node: IP: " << this->ip << "\n"
              << "Node: Port: " << this->port << std::endl;
    this->topic = UNDEFINED_TOPIC;
    this->nodeType = common::NodeType::DEFAULT;

    this->stay = true;
    this->ignoreOwnBroadcasts = false;

    this->solverManager = new SolverManager(this);

    this->nodeUtils = new NodeUtils(this);
    this->contactManager = new ContactManager(this);
    this->networkManager = new network::NetworkManager(this);
    this->pingManager = new PingManager(this);

    this->startThread = new std::thread(&Node::startNode, this);
    this->nodeEventThread = new std::thread(&Node::eventReceived, this);

    this->registry = nullptr;
    running = true;
}

Node::~Node()
{
    delete this->startThread;
    delete this->nodeEventThread;
    delete this->contactManager;
    delete this->netmask;
    delete this->ip;
    delete this->nodeUtils;
    delete this->solverManager;
    delete this->networkManager;
    delete this->pingManager;
}

void Node::shutdown()
{
    if (this->interactWithEureka) {
        this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
                OutEvent(common::MessageType::HTTP_REST, common::MessageRole::HTTPDEREGISTER, this->ip, this->port, this->uuid)));
        sleep(1);
    }
    this->stay = false;
    this->newEventCV.notify_all();
    this->startThread->join();
    this->nodeEventThread->join();
    this->unsetRegistry();
}

void Node::getOwnNetworkConfiguration()
{
    struct ifaddrs* interfaces = nullptr;
    int success = 0;
    success = getifaddrs(&interfaces);
    if (success == 0) {
        extractInterfaceInformation(interfaces, "en");
        if (this->ip == nullptr) {
            extractInterfaceInformation(interfaces, "wl");
        }
    }
    freeifaddrs(interfaces);
}

void Node::extractInterfaceInformation(ifaddrs* temp_addr, const std::string& interface)
{
    while (temp_addr != nullptr) {
        if (temp_addr->ifa_addr->sa_family == AF_INET) {
            if (strncmp(temp_addr->ifa_name, interface.c_str(), interface.length()) == 0) {
                this->ip = strdup(inet_ntoa(((struct sockaddr_in*) temp_addr->ifa_addr)->sin_addr));
                this->netmask = strdup(inet_ntoa(((struct sockaddr_in*) temp_addr->ifa_netmask)->sin_addr));
                return;
            }
        }
        temp_addr = temp_addr->ifa_next;
    }
    this->ip = nullptr;
}

std::string Node::getFreePort()
{
    std::random_device device;
    std::mt19937 generator(device());
    std::uniform_int_distribution<int> distribution(10000, 50000);
    std::string portString = std::to_string(distribution(generator));
    struct addrinfo* bindAddr;
    while (setupAddress(this->getIP(), const_cast<char*>(portString.c_str()), &bindAddr, SOCK_DGRAM, AF_INET) != 0) {
        portString = std::to_string(distribution(generator));
    }
    return portString;
}

void Node::setUuid()
{
    uuid_t tmpUuid;
    uuid_generate_time_safe(tmpUuid);
    char uuid_str[37];
    uuid_unparse(tmpUuid, uuid_str);
    std::stringstream ss;
    ss << uuid_str;
    this->uuid = ss.str();
}

void Node::notify()
{
    this->newEventCV.notify_all();
}

int Node::startNode()
{
    std::random_device device;
    std::mt19937 generator(device());
    std::uniform_int_distribution<int> distribution(5000000, 10000000);
    this->countBMessage = 0;
    while (this->stay) {
        if (!this->registry) {
            while (this->countBMessage < 2 && !this->registry) {
                std::cout << "Node: Trying to find a registry ..." << std::endl;
                this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
                        OutEvent(common::MessageType::BROADCAST, common::MessageRole::REGISTRY_REQUEST, this->ip, this->port, this->uuid, "Start Broadcast")));
                this->countBMessage += 1;
                usleep(distribution(generator));
            }
        } else {
            break;
        }
    }
    return 0;
}

std::string Node::startElection()
{
    std::cout << "#################################### ELECTION ##############################################" << std::endl;
    if (this->registry) {
        return this->registry->getUuid();
    }

    auto winningMessage = this->broadcastMessageList.front();
    for (auto& msg : this->broadcastMessageList) {
        if (winningMessage->getInformation().getUuid() < msg->getInformation().getUuid()) {
            winningMessage = msg;
        }
    }

    std::string winningUUID = winningMessage->getInformation().getUuid();
    this->registryAlive = true;
    if (winningUUID == this->uuid) {
        this->setRegistry(winningMessage->getInformation().getIP(), winningMessage->getInformation().getPort(), winningUUID, Node::UNDEFINED_TOPIC);
        this->nodeType = common::NodeType::REGISTRY;
        std::cout << "Node: I am the winner " << winningMessage->getInformation().toString() << std::endl;
        this->nodeUtils->getNetworkSolution(std::make_shared<common::InformationElement<node::InEvent>>(node::InEvent(common::MessageType::UNDEFINED_MSG_TYPE,
                                                    common::MessageRole::UNDEFINED_MSG_ROLE, this->ip, this->port, this->uuid)),
                this->solverManager->getNetworkSolverPair());
        if (this->interactWithEureka) {
            this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
                    OutEvent(common::MessageType::HTTP_REST, common::MessageRole::HTTPREGISTER, this->ip, this->port, this->uuid, "Registry")));
        }
    } else if (this->uuid < winningUUID) {
        this->nodeType = common::NodeType::LEAF;
        std::cout << "Node: winner: " << winningMessage->getInformation().toString() << std::endl;
    }
    this->broadcastMessageList.clear();
    this->pingManager->notify();
    return std::string(winningUUID);
}

void Node::unsetRegistry()
{
    this->registryAlive = false;
    this->registry = nullptr;
}

void Node::eventReceived()
{
    while (this->stay) {
        std::unique_lock<std::mutex> lk(this->newEventMutex);
        std::cout << "Node: waiting for incoming messages" << std::endl;
        this->newEventCV.wait(lk);
        lk.unlock();

        while (this->networkManager->inputEventAvailable()) {
            auto currentInputEvent = this->networkManager->getCurrentInputEvent();
            std::cout << "Node: Received message: " << currentInputEvent->getInformation().toString() << std::endl;
            std::cout << "Node: type: " << common::nodeTypes[this->nodeType] << ". Is registry set ? " << (!this->registry ? "no" : "yes") << std::endl;
            switch (currentInputEvent->getInformation().getType()) {
            case common::MessageType::BROADCAST:
                if (!this->registry) {
                    /**
                     * Received a registry request via broadcast
                     * No registry is know so far => election could be started
                     */
                    if ((currentInputEvent->getInformation().getRole() == common::MessageRole::REGISTRY_REQUEST)) {
                        this->broadcastMessageList.push_back(currentInputEvent);
                    }
                    /**
                     * Start with the Registry Election after 2 requests have been send and no response has been received
                     * If this node is elected a broadcast is sent to the participants of the election
                     */
                    if ((this->nodeType == common::NodeType::DEFAULT || this->nodeType == common::NodeType::LEAF) && this->countBMessage == 2) {
                        std::cout << "Node: going into election" << std::endl;
                        if (this->startElection() == this->uuid) {
                            std::cout << "Node: Broadcasting winner" << std::endl;
                            this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
                                    OutEvent(common::MessageType::BROADCAST, common::MessageRole::POST, this->ip, this->port, this->uuid)));
                        }
                    }
                    /**
                     * A Leaf receives a Post with registry information and has no registry yet
                     * It stores the information and starts its registration
                     */
                    if (currentInputEvent->getInformation().getRole() == common::MessageRole::POST && this->nodeType != common::NodeType::REGISTRY) {
                        this->registryAlive = true;
                        this->setRegistry(currentInputEvent->getInformation().getIP(), currentInputEvent->getInformation().getPort(),
                                currentInputEvent->getInformation().getUuid(), Node::UNDEFINED_TOPIC);
                        std::cout << "Node: solution: " << this->nodeUtils->getNetworkSolution(currentInputEvent, this->solverManager->getNetworkSolverPair())
                                  << std::endl;
                        // Put OutEvent registration to the output queue in the networkManager.
                        this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(OutEvent(common::MessageType::TCP,
                                common::MessageRole::REGISTER, currentInputEvent->getInformation().getIP(), currentInputEvent->getInformation().getPort(),
                                currentInputEvent->getInformation().getUuid(), "Leaf sends registration to the registry")));
                    }
                    if (currentInputEvent->getInformation().getRole() == common::MessageRole::ELECTION) {
                        std::cout << "Node: Election message received: " << currentInputEvent->getInformation().toString() << std::endl;
                        this->startThread->join();
                        delete this->startThread;
                        this->startThread = new std::thread(&Node::startNode, this);
                    }
                }
                /**
                * This node is the registry
                * It answers to incoming requests for its contact information
                */
                // TODO last part really neccessary?
                else if (currentInputEvent->getInformation().getRole() == common::MessageRole::REGISTRY_REQUEST &&
                            this->nodeType == common::NodeType::REGISTRY /*&&
                        currentInputEvent->getInformation().getUuid() != this->uuid*/) {
                    this->networkManager->addToOutputQueue(
                            std::make_shared<common::InformationElement<node::OutEvent>>(OutEvent(common::MessageType::TCP, common::MessageRole::POST,
                                    currentInputEvent->getInformation().getIP(), currentInputEvent->getInformation().getPort(), this->uuid)));
                }
                /**
                 * The registry receives its own Election broadcast
                 * The information is added to the knowledge base and further own broadcasts are ignored
                 */
                else if (currentInputEvent->getInformation().getRole() == common::MessageRole::POST && this->nodeType == common::NodeType::REGISTRY &&
                         !this->ignoreOwnBroadcasts) {
                    std::cout << "Node: own registry information is added to the knowledgebase." << std::endl;
                    this->nodeUtils->getNetworkSolution(currentInputEvent, this->solverManager->getNetworkSolverPair());
                    this->ignoreOwnBroadcasts = true;
                }
                /**
                 * A Leaf receives an Election message for its registry
                 * Another Leaf has detected that the registry is dead => a new one has to be elected
                 */
                else if (currentInputEvent->getInformation().getRole() == common::MessageRole::ELECTION &&
                         currentInputEvent->getInformation().getMessage() == this->registry->getUuid()) {
                    std::cout << "Node: Election message received: " << currentInputEvent->getInformation().toString() << std::endl;
                    this->startThread->join();
                    delete this->startThread;
                    this->startThread = new std::thread(&Node::startNode, this);
                } else if (this->nodeType == common::NodeType::LEAF) {
                    std::cout << "\033[1;31mNode: Ignoring broadcast since I am a leaf: " << currentInputEvent->getInformation().toString() << "\033[0m"
                              << std::endl;
                } else {

                    std::cout << "\033[1;31mNode: received a broadcast message with not supported MessageRole: "
                              << currentInputEvent->getInformation().toString() << "\033[0m" << std::endl;
                }
                break;

            case common::MessageType::TCP:
                /**
                 * A Registry already exists and responds to a request
                 */
                if (currentInputEvent->getInformation().getRole() == common::MessageRole::POST) {
                    this->registryAlive = true;
                    this->setRegistry(currentInputEvent->getInformation().getIP(), currentInputEvent->getInformation().getPort(),
                            currentInputEvent->getInformation().getUuid(), Node::UNDEFINED_TOPIC);
                    this->nodeType = common::NodeType::LEAF;
                    std::cout << "Node: registering as Leaf" << std::endl;
                    if (this->interactWithEureka) {
                        this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
                                OutEvent(common::MessageType::HTTP_REST, common::MessageRole::HTTPREGISTER, this->ip, this->port, this->uuid, "Leaf")));
                    }
                    this->nodeUtils->getNetworkSolution(
                            std::make_shared<common::InformationElement<node::InEvent>>(node::InEvent(
                                    common::MessageType::UNDEFINED_MSG_TYPE, common::MessageRole::UNDEFINED_MSG_ROLE, this->ip, this->port, this->uuid)),
                            this->solverManager->getNetworkSolverPair());
                    this->nodeUtils->getNetworkSolution(currentInputEvent, this->solverManager->getNetworkSolverPair());
                    std::cout << "Node: after registration: " << this->lastASPNetworkModel << std::endl;
                    this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(OutEvent(common::MessageType::TCP,
                            common::MessageRole::REGISTER, currentInputEvent->getInformation().getIP(), currentInputEvent->getInformation().getPort(),
                            currentInputEvent->getInformation().getUuid(), "Node: send registration to the registry")));
                }
                /**
                 * Registry receives a registration request and handles the information
                 */
                else if (currentInputEvent->getInformation().getRole() == common::MessageRole::REGISTER) {
                    // Put eventNode to the out put list in networkManager.
                    std::cout << "Node: leaf trying to register: " << currentInputEvent->getInformation().toString() << std::endl;
                    this->nodeUtils->getNetworkSolution(currentInputEvent, this->solverManager->getNetworkSolverPair());
                    this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(OutEvent(common::MessageType::TCP,
                            common::MessageRole::ACKNOWLEDGE, currentInputEvent->getInformation().getIP(), currentInputEvent->getInformation().getPort(),
                            currentInputEvent->getInformation().getUuid(), "Registration:" + currentInputEvent->getInformation().getUuid())));
                }
                /**
                 * Nodes receives an acknowledge for a request
                 */
                else if (currentInputEvent->getInformation().getRole() == common::MessageRole::ACKNOWLEDGE) {
                    std::cout << "Node: Acknowledge receive from  " << currentInputEvent->getInformation().toString() << std::endl;
                    if (currentInputEvent->getInformation().getMessage() == ("Registration:" + this->uuid)) {
                        this->nodeType = common::NodeType::LEAF;
                        this->nodeUtils->getNetworkSolution(std::make_shared<common::InformationElement<node::InEvent>>(node::InEvent(common::MessageType::TCP,
                                                                    common::MessageRole::ACKNOWLEDGE, this->ip, this->port, this->uuid)),
                                this->solverManager->getNetworkSolverPair());
                    }
                }
                /**
                 * A Leaf receives a Ping form its registry => registry is still reachable
                 */
                else if (currentInputEvent->getInformation().getRole() == common::MessageRole::PING && this->nodeType != common::NodeType::REGISTRY) {
                    this->registryAlive = true;
                    std::cout << "Node: Received Ping. Current Model: " << this->lastASPNetworkModel << std::endl;
                    if (!this->registry) {
                        this->setRegistry(currentInputEvent->getInformation().getIP(), currentInputEvent->getInformation().getPort(),
                                currentInputEvent->getInformation().getUuid(), Node::UNDEFINED_TOPIC);
                        this->nodeUtils->getNetworkSolution(currentInputEvent, this->solverManager->getNetworkSolverPair());
                    }
                    this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
                            OutEvent(common::MessageType::TCP, common::MessageRole::PING, currentInputEvent->getInformation().getIP(),
                                    currentInputEvent->getInformation().getPort(), currentInputEvent->getInformation().getUuid(), this->uuid)));

                } else if (currentInputEvent->getInformation().getRole() == common::MessageRole::PING && this->nodeType == common::NodeType::REGISTRY) {
                    this->pingManager->pingReceivedFromLeaf(this->contactManager->getContact(currentInputEvent->getInformation().getIP(),
                            currentInputEvent->getInformation().getPort(), currentInputEvent->getInformation().getMessage()));
                } else {
                    std::cout << "\033[1;31mNode: Node can not support registryMessage\033[0m" << std::endl;
                }
                break;
            case common::MessageType::ASP:
                switch (currentInputEvent->getInformation().getRole()) {
                case common::MessageRole::ASP_NETWORK_REQUEST:
                    /**
                     * React to the ASP request
                     * Registry => get leafs and forward the request to them
                     * Leaf => send current knowledgebase
                     */
                    if (this->nodeType == common::NodeType::REGISTRY) {
                        forwardEventToLeafs(currentInputEvent);
                    } else {
                        this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
                                OutEvent(common::MessageType::ASP, common::MessageRole::RESPONSE, currentInputEvent->getInformation().getIP(),
                                        currentInputEvent->getInformation().getPort(), currentInputEvent->getInformation().getUuid(),
                                        buildSolutionString(currentInputEvent, this->solverManager->getNetworkSolverPair()))));
                    }
                    break;
                case common::MessageRole::ASP_PUT: {
                    if (this->nodeType == common::NodeType::REGISTRY) {
                        forwardEventToLeafs(currentInputEvent);
                    } else {
                        std::string solution = this->nodeUtils->getSolution(currentInputEvent, this->solverManager->getKnowledgeBaseSolverPair());
                        if (solution.empty()) {
                            this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
                                    OutEvent(common::MessageType::ASP, common::MessageRole::FAILURE, currentInputEvent->getInformation().getIP(),
                                            currentInputEvent->getInformation().getPort(), currentInputEvent->getInformation().getUuid(), "ASP_PUT failed")));
                        } else {
                            this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
                                    OutEvent(common::MessageType::ASP, common::MessageRole::RESPONSE, currentInputEvent->getInformation().getIP(),
                                            currentInputEvent->getInformation().getPort(), currentInputEvent->getInformation().getUuid(), solution)));
                        }
                    }
                    break;
                }
                case common::MessageRole::ASP_GET:
                    if (this->nodeType == common::NodeType::REGISTRY) {
                        forwardEventToLeafs(currentInputEvent);
                    } else {
                        this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<node::OutEvent>>(
                                OutEvent(common::MessageType::ASP, common::MessageRole::RESPONSE, currentInputEvent->getInformation().getIP(),
                                        currentInputEvent->getInformation().getPort(), currentInputEvent->getInformation().getUuid(),
                                        buildSolutionString(currentInputEvent, this->solverManager->getKnowledgeBaseSolverPair()))));
                    }
                    break;
                default:
                    std::cout << "\033[1;31mNode: message not supported for ASP: " << common::messageRoles[currentInputEvent->getInformation().getRole()]
                              << "\033[0m" << std::endl;
                }

                break;
            case common::MessageType::UNDEFINED_MSG_TYPE:
            default:
                std::cout << "\033[1;31mNode:  Received Message with undefined type!\033[0m" << std::endl;
                break;
            }
        }
    }
}

std::string Node::buildSolutionString(const std::shared_ptr<const common::InformationElement<node::InEvent>> currentInputEvent,
        std::pair<common::SolverType, reasoner::asp::Solver*>* solverPair)
{
    std::stringstream solution;
    std::vector<std::string> result = this->nodeUtils->getFittingPredicates(currentInputEvent->getInformation().getMessage(), solverPair);
    for (std::string& predicate : result) {
        solution << predicate << " ";
    }
    solution << std::endl;
    return solution.str();
}

void Node::forwardEventToLeafs(const std::shared_ptr<const common::InformationElement<node::InEvent>> event)
{
    this->networkManager->setLeafs(this->nodeUtils->getLeafs());
    this->networkManager->addToOutputQueue(std::make_shared<common::InformationElement<OutEvent>>(
            OutEvent(event->getInformation().getType(), event->getInformation().getRole(), event->getInformation().getIP(), event->getInformation().getPort(),
                    event->getInformation().getUuid(), event->getInformation().getMessage())));
}

void Node::nodeSigintHandler(int sig)
{
    std::cout << "Node: Caught SIGINT(" << sig << ")! Terminating ..." << std::endl;
    running = false;
}

ContactManager* Node::getContactManager() const
{
    return this->contactManager;
}

std::string Node::getTopic() const
{
    return this->topic;
}

void Node::setTopic(std::string topic)
{
    this->topic = topic;
}

void Node::setLastASPNetworkModel(std::string solution)
{
    this->lastASPNetworkModel = solution;
}

void Node::setLastASPKnowledgeBaseModel(std::string solution)
{
    this->lastASPKnowledgeBaseModel = solution;
}

char* Node::getIP()
{
    return this->ip;
}

char* Node::getNetmask()
{
    return this->netmask;
}

void Node::setRegistry(std::string ip, std::string port, std::string uuid, std::string topic)
{
    this->registry = this->contactManager->getContact(ip, port, uuid, topic);
    this->networkManager->registry = this->registry;
    this->pingManager->notify();
}

common::NodeType Node::getNodeType()
{
    return this->nodeType;
}

Contact* Node::getRegistry() const
{
    return this->registry;
}

std::string Node::getPort()
{
    return this->port;
}

network::NetworkManager* Node::getNetworkManager() const
{
    return this->networkManager;
}

SolverManager* Node::getSolverManager() const
{
    return this->solverManager;
}

void Node::setRegistryAlive(bool alive)
{
    this->registryAlive = alive;
}

std::string Node::getUuid()
{
    return this->uuid;
}

bool Node::isRunning()
{
    return running;
}

void Node::setInteractWithEureka(bool interactWithEureka)
{
    this->interactWithEureka = interactWithEureka;
}

bool Node::isInteractWithEureka() const
{
    return this->interactWithEureka;
}

std::string& Node::getLastASPNetworkModel()
{
    return this->lastASPNetworkModel;
}

std::string& Node::getLastASPKnowledgeBaseModel()
{
    return this->lastASPNetworkModel;
}

bool Node::isStay() const
{
    return stay;
}

} // namespace node
} // namespace knowledge_registry