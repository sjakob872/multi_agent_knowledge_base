#include "knowledge_registry/node/ContactManager.h"
#include "knowledge_registry/node/Contact.h"

#include "knowledge_registry/node/Node.h"

namespace knowledge_registry
{
namespace node
{
ContactManager::ContactManager(Node *node)
    : node(node)
{
    this->ownContact =
        new Contact(std::string(this->node->getIP()), std::string(this->node->getPort()), this->node->getUuid(), this->node->getTopic());
    this->contacts.emplace(this->node->getUuid(), this->ownContact);
}
ContactManager::~ContactManager()
{
    for (auto &pair : this->contacts)
    {
        delete pair.second;
    }
}
Contact *ContactManager::getContact(std::string ip, std::string port, std::string uuid, std::string topic)
{
    auto contact = this->contacts.find(uuid);
    if (contact != this->contacts.end())
    {
        return contact->second;
    }
    Contact *ret = new Contact(ip, port, uuid, topic);
    this->contacts.emplace(uuid, ret);
    return ret;
}

Contact *ContactManager::getOwnContact()
{
    return this->ownContact;
}
} // namespace node
} // namespace knowledge_registry
