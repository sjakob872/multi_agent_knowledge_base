#include "knowledge_registry/network/receiver/Receiver.h"

#include "knowledge_registry/common/JsonSerializer.h"
#include "knowledge_registry/network/NetworkManager.h"
#include "knowledge_registry/node/event/InEvent.h"

#include <arpa/inet.h>

namespace knowledge_registry
{
namespace network
{
Receiver::Receiver(NetworkManager* networkManager)
{
    this->stay = true;
    this->networkManager = networkManager;
}

// Collect Ip message
int Receiver::receive(int socketFD, const std::string& receiverName)
{
    struct sockaddr_storage recvAddr;
    socklen_t recvAddrLen = sizeof(struct sockaddr_storage);
    ssize_t ret = 0;
    CoapPDU recvPDU;

    while (true) {
        recvPDU = CoapPDU((uint8_t*) buffer, BUF_LEN, BUF_LEN);
        std::cout << receiverName << ": inside main loop. Waiting for incoming message." << std::endl;
        ret = recvfrom(socketFD, &buffer, BUF_LEN, 0, (sockaddr*) &recvAddr, &recvAddrLen);
        if(!this->stay) {
            break;
        }
        if (ret == -1) {
            std::cout << "\033[1;31m" << receiverName << ": Error receiving data" << "\033[0m" << std::endl;
            continue;
        }
        recvPDU.setPDULength(ret);
        if (recvPDU.validate() != 1) {
            std::cout << "\033[1;31m" << receiverName << ": Malformed CoAP packet" << "\033[0m" << std::endl;
            continue;
        }
        std::string input = std::string((char*) recvPDU.getPayloadCopy());
        node::InEvent temp = node::InEvent(common::MessageType::UNDEFINED_MSG_TYPE, common::MessageRole::UNDEFINED_MSG_ROLE, "", "", "");
        common::JsonSerializer::Deserialize(&temp, input);
        std::shared_ptr<common::InformationElement<node::InEvent>> event = std::make_shared<common::InformationElement<node::InEvent>>(temp);
        this->networkManager->addToInputQueue(event);
    }
    return ret;
}

bool Receiver::haveInput(int fd, double timeout)
{
    int status;
    fd_set fds;
    struct timeval tv;
    FD_ZERO(&fds);
    FD_SET(fd, &fds);
    tv.tv_sec = (long) timeout;                            // cast needed for C++
    tv.tv_usec = (long) ((timeout - tv.tv_sec) * 1000000); // 'suseconds_t'

    while (true) {
        if (!(status = select(fd + 1, &fds, 0, 0, &tv))) {
            return false;
        } else if (status > 0 && FD_ISSET(fd, &fds)) {
            return true;
        } else if (status > 0) {
            std::cout << "\033[1;31mReceiver: I am confused\033[0m" << std::endl;
        } else if (errno != EINTR) {
            std::cout << "\033[1;31mReceiver: select\033[0m" << std::endl; // tbd EBADF: man page "an error has occurred"
        }
    }
}

bool Receiver::flushSocketBeforeClose(int fd, double timeout)
{
    const double start = time(nullptr);
    char discard[99];
    assert(SHUT_WR == 1);
    if (shutdown(fd, 1) != -1) {
        while (time(nullptr) < start + timeout) {
            while (haveInput(fd, 0.01)) // can block for 0.01 secs
            {
                if (!read(fd, discard, sizeof discard)) {
                    return true; // success!
                }
            }
        }
    }
    return false;
}

int Receiver::getSO_ERROR(int fd)
{
    int err = 1;
    socklen_t len = sizeof err;
    if (-1 == getsockopt(fd, SOL_SOCKET, SO_ERROR, (char*) &err, &len)) {
        std::cout << "\033[1;31mReceiver: getSO_ERROR\033[0m" << std::endl;
    }
    if (err) {
        errno = err; // set errno to the socket SO_ERROR
    }
    return err;
}

void Receiver::closeSocket(int fd)
{
    if (fd >= 0) {
        getSO_ERROR(fd);                 // first clear any errors, which can cause close to fail
        if (shutdown(fd, SHUT_RDWR) < 0) // secondly, terminate the 'reliable' delivery
        {
            if (errno != ENOTCONN && errno != EINVAL) // SGI causes EINVAL
            {
                perror("\033[1;31mReceiver: shutdown\033[0m");
            }
        }
        if (close(fd) < 0) // finally call close()
        {
            perror("\033[1;31mReceiver: close\033[0m");
        }
    }
}
} // namespace network
} // namespace knowledge_registry