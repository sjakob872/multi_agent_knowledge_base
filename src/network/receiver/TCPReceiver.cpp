#include "knowledge_registry/network/receiver/TCPReceiver.h"

#include "knowledge_registry/network/NetworkManager.h"
#include "knowledge_registry/node/Contact.h"

#include <cantcoap/nethelper.h>

#include <netdb.h>

namespace knowledge_registry
{
namespace network
{
TCPReceiver::TCPReceiver(NetworkManager* networkManager)
        : Receiver(networkManager)
{
    this->socketFD = -1;
    this->receiveIpThread = new std::thread(&TCPReceiver::receiveTCP, this);
}

TCPReceiver::~TCPReceiver()
{
    this->stay = false;
    flushSocketBeforeClose(this->socketFD, 2.0); // can block for 2s
    closeSocket(this->socketFD);
    this->receiveIpThread->join();
    delete this->receiveIpThread;
}

int TCPReceiver::receiveTCP()
{
    struct addrinfo* bindAddr;
    if (setupAddress(const_cast<char*>(this->networkManager->getNodeAddress()->getIP().c_str()),
                const_cast<char*>(networkManager->getNodeAddress()->getPort().c_str()), &bindAddr, SOCK_DGRAM, AF_INET) != 0) {
        std::cout << "\033[1;31mTCPReceiver: Error setting up bind address, exiting.\033[0m" << std::endl;
    }
    int tcp = 0;
    this->socketFD = socket(bindAddr->ai_family, bindAddr->ai_socktype, bindAddr->ai_protocol);
    if (setsockopt(this->socketFD, SOL_SOCKET, SO_REUSEADDR, &tcp, sizeof tcp) == -1) {
        std::cout << "\033[1;31mTCPReceiver: Error setting up socket options\033[0m" << std::endl;
    }
    if (setsockopt(this->socketFD, SOL_SOCKET, SO_REUSEPORT, &tcp, sizeof tcp) == -1) {
        std::cout << "\033[1;31mBroadCastReceiver: Error setting up socket options SO_REUSEPORT\033[0m" << std::endl;
    }
    if (bind(this->socketFD, bindAddr->ai_addr, bindAddr->ai_addrlen) != 0) {
        std::cout << "TCPReceiver: Error Binding : " << bindAddr->ai_addr << std::endl;
        if (connect(this->socketFD, bindAddr->ai_addr, bindAddr->ai_addrlen) != 0) {
            std::cout << "\033[1;31mTCPReceiver: Error Connecting socket\033[0m" << std::endl;
        } else {
            std::cout << "TCPReceiver: Successfully connected to socket" << std::endl;
        }
    }
    this->receive(this->socketFD, "TCPReceiver");
    return 0;
}
} // namespace network
} // namespace knowledge_registry