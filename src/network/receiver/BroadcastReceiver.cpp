#include "knowledge_registry/network/receiver/BroadcastReceiver.h"

#include "knowledge_registry/network/NetworkManager.h"

#include <cantcoap/nethelper.h>
#include <netdb.h>

namespace knowledge_registry
{
namespace network
{
BroadcastReceiver::BroadcastReceiver(NetworkManager* networkManager)
        : Receiver(networkManager)
{
    this->broadcastSocketFD = -1;
    this->receiveBroadcastThread = new std::thread(&BroadcastReceiver::receiveBroadcast, this);
}

BroadcastReceiver::~BroadcastReceiver()
{
    this->stay = false;
    flushSocketBeforeClose(this->broadcastSocketFD, 2.0); // can block for 2s
    closeSocket(this->broadcastSocketFD);
    this->receiveBroadcastThread->join();
    delete this->receiveBroadcastThread;
}

int BroadcastReceiver::receiveBroadcast()
{
    struct addrinfo* bindAddr;
    int ret = setupAddress(const_cast<char *>(this->networkManager->getBroadCastAddress().c_str()), const_cast<char *>(this->networkManager->getBroadcastPort().c_str()), &bindAddr,
            SOCK_DGRAM, AF_INET);
    if (ret != 0) {
        std::cout << "\033[1;31mBroadCastReceiver: Error setting up bind address, exiting.\033[0m" << std::endl;
        return -1;
    }

    int broadCast = 1;
    this->broadcastSocketFD = socket(AF_INET, SOCK_DGRAM, 0);
    if (setsockopt(this->broadcastSocketFD, SOL_SOCKET, SO_REUSEADDR, &broadCast, sizeof broadCast) == -1) {
        std::cout << "\033[1;31mBroadCastReceiver: Error setting up socket options SO_REUSEADDR\033[0m" << std::endl;
    }
    if (setsockopt(this->broadcastSocketFD, SOL_SOCKET, SO_REUSEPORT, &broadCast, sizeof broadCast) == -1) {
        std::cout << "\033[1;31mBroadCastReceiver: Error setting up socket options SO_REUSEPORT\033[0m" << std::endl;
    }

    if (bind(this->broadcastSocketFD, bindAddr->ai_addr, bindAddr->ai_addrlen) != 0) {
        std::cout << "\033[1;31mBroadcastReceiver: Error Binding : " << bindAddr->ai_addr << "\033[0m" <<std::endl;
        int error = connect(this->broadcastSocketFD, bindAddr->ai_addr, bindAddr->ai_addrlen);
        if ( error != 0) {
            std::cout << "\033[1;31mBroadCastReceiver: Error Connecting socket "  << error << "\033[0m" << std::endl;
        } else {
            std::cout << "BroadCastReceiver: Successfully connected to socket" << std::endl;
        }
    }
    return this->receive(this->broadcastSocketFD, "BroadcastReceiver");
}
} // namespace network
} // namespace knowledge_registry