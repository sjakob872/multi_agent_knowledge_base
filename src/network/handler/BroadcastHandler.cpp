#include "knowledge_registry/network/handler/BroadcastHandler.h"

#include "knowledge_registry/network/NetworkManager.h"
#include "knowledge_registry/node/event/OutEvent.h"

#include <cantcoap/nethelper.h>
#include <netdb.h>

namespace knowledge_registry
{
namespace network
{
BroadcastHandler::BroadcastHandler(NetworkManager* networkManager)
        : Handler(networkManager)
{
    this->broadcastSocketFD = -1;
    this->prepareSocket();
}

bool BroadcastHandler::isResponsible(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{
    return event->getInformation().getType() == common::MessageType::BROADCAST;
}

void BroadcastHandler::handleEvent(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{

    switch (event->getInformation().getRole()) {
    case common::MessageRole::REGISTRY_REQUEST:
    case common::MessageRole::POST: // was sendPost before
        sendBroadcast(event);
        break;
    case common::MessageRole::ELECTION:
        sendBroadcast(event);
        this->networkManager->unsetRegistry();
        break;
    default:
        std::cout << "\033[1;31mBroadcastHandler: Unsupported event: " << event->getInformation().toString() << "\033[0m" << std::endl;
    }
}

void BroadcastHandler::sendBroadcast(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{
    struct addrinfo* remoteAddress;
    if (setupAddress(const_cast<char*>(this->networkManager->getBroadCastAddress().c_str()),
                const_cast<char*>(this->networkManager->getBroadcastPort().c_str()), &remoteAddress, SOCK_DGRAM, AF_INET) != 0) {
        std::cout << "\033[1;31mBroadcastHandler: Error setting up remote address, exiting.\033[0m" << std::endl;
    }
    if (connect(this->broadcastSocketFD, remoteAddress->ai_addr, remoteAddress->ai_addrlen) != 0) {
        std::cout << "\033[1;31mBroadcastHandler: Error connecting to remote host.\033[0m" << std::endl;
    }

    CoapPDU* pdu = generatePDU(event, true);
    if (send(this->broadcastSocketFD, pdu->getPDUPointer(), pdu->getPDULength(), 0) != pdu->getPDULength()) {
        std::cout << "\033[1;31mBroadcastHandler: Error sending message.\033[0m" << std::endl;
    }
    delete pdu;
}

int BroadcastHandler::prepareSocket()
{
    int broadcast = 1;
    this->broadcastSocketFD = socket(AF_INET, SOCK_DGRAM, 0);

    if (setsockopt(this->broadcastSocketFD, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof broadcast) == -1) {
        std::cout << "\033[1;31mBroadcastHandler: error setting socket options\033[0m" << std::endl;
    }
    this->networkManager->setBroadcastSocket(this->broadcastSocketFD);

    return 0;
}
} // namespace network
} // namespace knowledge_registry