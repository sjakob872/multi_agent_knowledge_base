#include "knowledge_registry/network/handler/ASPRequestHandler.h"

#include "knowledge_registry/network/NetworkManager.h"
#include "knowledge_registry/node/Contact.h"
#include "knowledge_registry/node/event/OutEvent.h"

namespace knowledge_registry
{
namespace network
{
ASPRequestHandler::ASPRequestHandler(NetworkManager* networkManager)
        : Handler(networkManager)
{
}

bool ASPRequestHandler::isResponsible(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{
    return event->getInformation().getType() == common::MessageType::ASP;
}

void ASPRequestHandler::handleEvent(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{
    auto nodeAddress = this->networkManager->getNodeAddress();

    switch (event->getInformation().getRole()) {
    case common::MessageRole::ASP_NETWORK_REQUEST:
    case common::MessageRole::ASP_PUT:
    case common::MessageRole::ASP_GET:
        if (this->networkManager->getNodeType() == common::NodeType::REGISTRY) {
            auto leafs = this->networkManager->getLeafs();
            for (auto leaf : leafs) {
                sendMessage(const_cast<char*>("5050"), leaf->getIP(), leaf->getPort(), event);
            }
            if (leafs.empty()) {
                sendMessage(const_cast<char*>("5050"), event->getInformation().getIP(), event->getInformation().getPort(),
                        std::make_shared<common::InformationElement<node::OutEvent>>(node::OutEvent(event->getInformation().getType(),
                                common::MessageRole::RESPONSE, nodeAddress->getIP(), nodeAddress->getPort(), nodeAddress->getUuid(), "No Leafs registered!")));
            }
        } else if (this->networkManager->getNodeType() == common::NodeType::DEFAULT) {
            sendMessage(const_cast<char*>("5050"), event->getInformation().getIP(), event->getInformation().getPort(),
                    std::make_shared<common::InformationElement<node::OutEvent>>(
                            node::OutEvent(event->getInformation().getType(), event->getInformation().getRole(), nodeAddress->getIP(), nodeAddress->getPort(),
                                    nodeAddress->getUuid(), event->getInformation().getMessage())));
        } else {
            std::cout << "\033[1;31mASPRequestHandler: ASP request send to Leaf \033[0m" << std::endl;
        }
        break;
    case common::MessageRole::RESPONSE:
    case common::MessageRole::FAILURE:
        sendMessage(const_cast<char*>("8970"), event->getInformation().getIP(), event->getInformation().getPort(),
                std::make_shared<common::InformationElement<node::OutEvent>>(
                        node::OutEvent(event->getInformation().getType(), event->getInformation().getRole(), nodeAddress->getIP(), nodeAddress->getPort(),
                                nodeAddress->getUuid(), event->getInformation().getMessage())));
        break;
    default:
        std::cout << "\033[1;31mASPRequestHandler: not supported \033[0m" << std::endl;
    }
}

} // namespace network
} // namespace knowledge_registry
