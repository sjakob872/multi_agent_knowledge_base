#include "knowledge_registry/network/handler/TCPHandler.h"

#include "knowledge_registry/network/NetworkManager.h"
#include "knowledge_registry/node/event/OutEvent.h"
#include "knowledge_registry/node/Contact.h"

namespace knowledge_registry
{
namespace network
{
TCPHandler::TCPHandler(NetworkManager* networkManager)
        : Handler(networkManager)
{
}

bool TCPHandler::isResponsible(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{
    return event->getInformation().getType() == common::MessageType::TCP;
}

void TCPHandler::handleEvent(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{
    auto nodeAddress = this->networkManager->getNodeAddress();
    auto registry = this->networkManager->getRegistry();
    std::string port = this->networkManager->getFreePort();
    switch (event->getInformation().getRole()) {
    case common::MessageRole::REGISTER:
        sendMessage(port, event->getInformation().getIP(), event->getInformation().getPort(),
                std::make_shared<common::InformationElement<node::OutEvent>>(node::OutEvent(common::MessageType::TCP, common::MessageRole::REGISTER,
                        nodeAddress->getIP(), nodeAddress->getPort(), nodeAddress->getUuid(), event->getInformation().getMessage())));
        break;
    case common::MessageRole::POST:
        sendMessage(port, event->getInformation().getIP(), event->getInformation().getPort(),
                std::make_shared<common::InformationElement<node::OutEvent>>(node::OutEvent(common::MessageType::TCP, common::MessageRole::POST,
                        registry->getIP(), registry->getPort(), registry->getUuid(), event->getInformation().getMessage())));
        break;
    case common::MessageRole::ACKNOWLEDGE:
        sendMessage(port, event->getInformation().getIP(), event->getInformation().getPort(),
                std::make_shared<common::InformationElement<node::OutEvent>>(
                        node::OutEvent(event->getInformation().getType(), event->getInformation().getRole(), nodeAddress->getIP(), nodeAddress->getPort(),
                                nodeAddress->getUuid(), event->getInformation().getMessage())));
        break;
    case common::MessageRole::PING:
        sendMessage(port, event->getInformation().getIP(), event->getInformation().getPort(),
                std::make_shared<common::InformationElement<node::OutEvent>>(node::OutEvent(common::MessageType::TCP, common::MessageRole::PING,
                        registry->getIP(), registry->getPort(), registry->getUuid(), event->getInformation().getMessage())));
        break;
    default:
        std::cout << "TCPHandler: Unsupported MessageRole: " << event->getInformation().getRole() << std::endl;
    }
}
} // namespace network
} // namespace knowledge_registry