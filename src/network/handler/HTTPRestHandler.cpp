#include "knowledge_registry/network/handler/HTTPRestHandler.h"

#include "knowledge_registry/common/KREnums.h"
#include "knowledge_registry/network/NetworkManager.h"
#include "knowledge_registry/node/event/OutEvent.h"
#include "knowledge_registry/node/Contact.h"

#include <jsoncpp/json/reader.h>
#include <sstream>

namespace knowledge_registry
{

namespace network
{

HTTPRestHandler::HTTPRestHandler(NetworkManager* networkManager)
        : Handler(networkManager)
{
}

bool HTTPRestHandler::isResponsible(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{
    return event->getInformation().getType() == common::MessageType::HTTP_REST;
}

void HTTPRestHandler::handleEvent(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{
    std::cout << "HTTPRestHandler: handle event called!" << std::endl;
    if (event->getInformation().getRole() == common::MessageRole::HTTPREGISTER) {
        this->registerAtEurekaServer(event);
    } else if (event->getInformation().getRole() == common::MessageRole::HTTPDEREGISTER) {
        this->deregisterAtEurekaServer(event);
    }
}

std::size_t HTTPRestHandler::callback(const char* in, std::size_t size, std::size_t num, std::string* out)
{
    const std::size_t totalBytes(size * num);
    out->append(in, totalBytes);
    return totalBytes;
}

void HTTPRestHandler::registerAtEurekaServer(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{
    std::stringstream ss;
    ss << "{\n"
       << "    \"instance\": {\n"
       << "        \"hostName\": \"localhost\",\n"
       << "        \"app\": \"" << event->getInformation().getUuid() << "\",\n"
       << "        \"vipAddress\": \"" << event->getInformation().getUuid() << "\",\n"
       << "        \"instanceId\": \"" << event->getInformation().getMessage() << ":" << event->getInformation().getIP() << ":"
       << event->getInformation().getPort() << ":" << event->getInformation().getUuid() << "\",\n"
       << "        \"ipAddr\": \"" << event->getInformation().getIP() << "\",\n"
       << "        \"status\": \"UP\",\n"
       << "        \"port\": {\n"
       << "            \"$\": " << event->getInformation().getPort() << ",\n"
       << "            \"@enabled\": true\n"
       << "        },\n"
       << "        \"dataCenterInfo\": {\n"
       << "            \"@class\": \"com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo\",\n"
       << "            \"name\": \"MyOwn\"\n"
       << "        }\n"
       << "    }\n"
       << "}";
    httpPost("http://localhost:8761/eureka/apps/" + this->networkManager->getNodeAddress()->getUuid(), ss.str());
}

void HTTPRestHandler::deregisterAtEurekaServer(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{
    std::string response = httpGet("http://localhost:8761/eureka/apps/" + event->getInformation().getUuid());
    Json::Value root;
    Json::Reader reader;
    bool parsingSuccessful = reader.parse(response, root);
    if (!parsingSuccessful) {
        std::cout << "\033[1;31mNodeUtils: Error parsing the string: " << response << "\033[0m" << std::endl;
    }
    const Json::Value applicationID = root["application"]["instance"];
    httpDelete("http://localhost:8761/eureka/apps/" + event->getInformation().getUuid() + "/" + applicationID[0]["instanceId"].asString());
}

std::string HTTPRestHandler::httpGet(const std::string& url)
{
    curl_slist* headers = nullptr;
    auto httpData = new std::string();
    CURL* curl = generateCurlRequest(url, headers, httpData);
    int httpCode(0);
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
    std::string tmp = std::string(*httpData);
    curl_easy_cleanup(curl);
    curl_slist_free_all(headers);
    delete httpData;
    return tmp;
}

void HTTPRestHandler::httpDelete(const std::string& url)
{
    curl_slist* headers = nullptr;
    auto httpData = new std::string();
    CURL* curl = generateCurlRequest(url, headers, httpData);
    int httpCode(0);
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
    curl_easy_cleanup(curl);
    curl_slist_free_all(headers);
    delete httpData;
}

void HTTPRestHandler::httpPost(const std::string& url, const std::string& request)
{
    curl_slist* headers = nullptr;
    auto httpData = new std::string();
    CURL* curl = generateCurlRequest(url, headers, httpData);
    // Response information.
    int httpCode(0);
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, request.c_str());
    curl_easy_perform(curl);
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
    curl_easy_cleanup(curl);
    curl_slist_free_all(headers);
    std::cout << "HTTPRestHandler::httpPost: " << httpCode << std::endl;
    delete httpData;
}

CURL* HTTPRestHandler::generateCurlRequest(const std::string& url, curl_slist* headers, std::string* httpData) const
{
    CURL* curl = curl_easy_init();
    headers = curl_slist_append(headers, "Accept: application/json");
    headers = curl_slist_append(headers, "Content-Type: application/json");
    headers = curl_slist_append(headers, "charsets: utf-8");
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
    // Don't bother trying IPv6, which would increase DNS resolution time.
    curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    // Don't wait forever, time out after 10 seconds.
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);
    // Follow HTTP redirects if necessary.
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, HTTPRestHandler::callback);
    // Hook up data container (will be passed as the last parameter to the
    // callback handling function).  Can be any pointer type, since it will
    // internally be passed as a void pointer.
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData);
    // Set remote URL.
    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    return curl;
}
} // namespace network
} // namespace knowledge_registry