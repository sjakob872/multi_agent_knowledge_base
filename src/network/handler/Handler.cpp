#include "knowledge_registry/network/handler/Handler.h"

#include "knowledge_registry/common/JsonSerializer.h"
#include "knowledge_registry/network/NetworkManager.h"
#include "knowledge_registry/node/Contact.h"
#include "knowledge_registry/common/InformationElement.h"
#include "knowledge_registry/node/event/OutEvent.h"

#include <cantcoap/nethelper.h>
#include <netdb.h>

namespace knowledge_registry
{
namespace network
{
Handler::Handler(NetworkManager* networkManager)
{
    this->networkManager = networkManager;
}

void Handler::sendMessage(const std::string& sendPort, const std::string& remoteIP, const std::string& remotePort, const std::shared_ptr<const common::InformationElement<node::OutEvent>> event)
{
    // setup bind address
    struct addrinfo* bindAddr;
    int ret = setupAddress(const_cast<char *>(this->networkManager->getNodeAddress()->getIP().c_str()), const_cast<char *>(sendPort.c_str()), &bindAddr, SOCK_DGRAM, AF_INET);
    if (ret != 0) {
        std::cout << "\033[1;31mHandler: Error binding address.\033[0m" << std::endl;
    }

    // setup socket
    int socketFD = socket(bindAddr->ai_family, bindAddr->ai_socktype, bindAddr->ai_protocol);
    struct addrinfo* remoteAddress;

    ret = setupAddress(const_cast<char*>(remoteIP.c_str()), const_cast<char*>(remotePort.c_str()), &remoteAddress, SOCK_DGRAM, AF_INET);
    // printAddress(remoteAddress);
    if (ret != 0) {
        std::cout << "\033[1;31mHandler: Error setting up remote address to send nodeEvent.\033[0m" << std::endl;
    }

    // call connect to associate remote address with socket
    ret = connect(socketFD, remoteAddress->ai_addr, remoteAddress->ai_addrlen);
    if (ret != 0) {
        std::cout << "\033[1;31mHandler: Error: " << gai_strerror(ret) << " Error connecting to remote host.\033[0m" << std::endl;
    }

    CoapPDU* pdu = generatePDU(event, false);

    //     send packet
    ret = send(socketFD, pdu->getPDUPointer(), pdu->getPDULength(), 0);
    if (ret != pdu->getPDULength()) {
        std::cout << "\033[1;31mHandler: Error sending message.\033[0m" << std::endl;
    } else {
        std::cout << "Handler: Message sent" << std::endl;
    }
    close(socketFD);
    delete pdu;
}

CoapPDU* Handler::generatePDU(const std::shared_ptr<const common::InformationElement<node::OutEvent>> event, bool confirm)
{
    std::string output;
    auto info = event->getInformation();
    common::JsonSerializer::Serialize(info, output);
    auto pdu = new CoapPDU();
    pdu->setVersion(1);
    if(confirm) {
        pdu->setType(CoapPDU::COAP_CONFIRMABLE);
    }
    pdu->setType(CoapPDU::COAP_REGISTRATION);
    pdu->setCode(CoapPDU::COAP_POST);
    pdu->setToken((uint8_t*) "\3\2\1\1", 4);
    pdu->setMessageID(0x0005);
    pdu->setURI((char*) "test", 4);
    pdu->addOption(CoapPDU::COAP_OPTION_CONTENT_FORMAT, 1, (uint8_t*) ")");
    pdu->setPayload((uint8_t*) output.c_str(), output.length());
    return pdu;
}
} // namespace network
} // namespace knowledge_registry
