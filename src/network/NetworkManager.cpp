#include "knowledge_registry/network/NetworkManager.h"

#include "knowledge_registry/network/handler/ASPRequestHandler.h"
#include "knowledge_registry/network/handler/BroadcastHandler.h"
#include "knowledge_registry/network/handler/HTTPRestHandler.h"
#include "knowledge_registry/network/handler/TCPHandler.h"
#include "knowledge_registry/network/receiver/BroadcastReceiver.h"
#include "knowledge_registry/network/receiver/TCPReceiver.h"
#include "knowledge_registry/node/Contact.h"
#include "knowledge_registry/node/ContactManager.h"
#include "knowledge_registry/node/Node.h"
#include "knowledge_registry/node/event/InEvent.h"
#include "knowledge_registry/node/event/OutEvent.h"

#include <arpa/inet.h>

namespace knowledge_registry
{
namespace network
{
NetworkManager::NetworkManager(node::Node* node)
        : eventInputQueue(NetworkManager::queueSize)
        , eventOutputQueue(NetworkManager::queueSize)
{
    this->node = node;
    this->stay = true;
    this->registry = nullptr;
    this->broadcastSocket = -1;
    this->broadCastAddress = calculateBroadcastAddress();
    this->broadcastPort = "4446";

    this->handlers.emplace_back(new ASPRequestHandler(this));
    this->handlers.emplace_back(new TCPHandler(this));
    this->handlers.emplace_back(new BroadcastHandler(this));
    if (this->node->isInteractWithEureka()) {
        this->handlers.emplace_back(new HTTPRestHandler(this));
    }

    this->tcpReceiver = new TCPReceiver(this);
    this->broadcastReceiver = new BroadcastReceiver(this);

    this->checkResponsibleThread = new std::thread(&NetworkManager::checkResponsible, this);
    this->checkInputQueueThread = new std::thread(&NetworkManager::checkInputQueue, this);
}

NetworkManager::~NetworkManager()
{
    for (Handler* handler : this->handlers) {
        delete handler;
    }
    this->stay = false;
    this->outputQueueCV.notify_all();
    this->checkResponsibleThread->join();
    this->inputQueueCV.notify_all();
    this->checkInputQueueThread->join();
    delete this->checkResponsibleThread;
    delete this->checkInputQueueThread;
    delete this->broadcastReceiver;
    delete this->tcpReceiver;
}

void NetworkManager::addToOutputQueue(std::shared_ptr<common::InformationElement<node::OutEvent>> event)
{
    this->eventOutputQueue.add(event);
    this->outputQueueCV.notify_one();
}

void NetworkManager::addToInputQueue(std::shared_ptr<common::InformationElement<node::InEvent>> event)
{
    this->eventInputQueue.add(event);
    this->inputQueueCV.notify_one();
}

void NetworkManager::checkResponsible()
{
    while (this->stay) {
        std::unique_lock<std::mutex> lk(this->outputQueueMutex);
        std::cout << "NetworkManager: waiting for incoming Events." << std::endl;
        this->outputQueueCV.wait(lk);
        while (!this->eventOutputQueue.empty()) {
            const std::shared_ptr<const common::InformationElement<node::OutEvent>> event = this->eventOutputQueue.getFirst();
            std::cout << "NetworkManager: event: " << event->getInformation().toString() << std::endl;
            for (auto& handler : this->handlers) {
                if (handler->isResponsible(event)) {
                    handler->handleEvent(event);
                    break;
                }
            }
        }
    }
}

std::string NetworkManager::calculateBroadcastAddress()
{
    char* host_ip = this->node->getIP();
    char* netmask = this->node->getNetmask();
    struct in_addr host;
    struct in_addr mask;
    struct in_addr broadcast;
    char broadcast_address[INET_ADDRSTRLEN];
    if (inet_pton(AF_INET, host_ip, &host) == 1 && inet_pton(AF_INET, netmask, &mask) == 1) {
        broadcast.s_addr = host.s_addr | ~mask.s_addr;
    } else {
        std::cout << "\033[1;31mNetworkManager: cant calculate broadcast address!\033[0m" << std::endl;
        return "";
    }
    if (inet_ntop(AF_INET, &broadcast, broadcast_address, INET_ADDRSTRLEN) != nullptr) {
        return std::string(broadcast_address);
    } else {
        std::cout << "\033[1;31mNetworkManager: cant calculate broadcast address!\033[0m" << std::endl;
        return "";
    }
}

void NetworkManager::checkInputQueue()
{
    while (this->stay) {
        std::unique_lock<std::mutex> lk(this->inputQueueMutex);
        std::cout << "NetworkManager: waiting for incoming Messages." << std::endl;
        this->inputQueueCV.wait(lk);
        this->node->notify();
        lk.unlock();
    }
}

std::string NetworkManager::getFreePort()
{
    return this->node->getFreePort();
}

std::shared_ptr<const common::InformationElement<node::InEvent>> NetworkManager::getCurrentInputEvent()
{
    return this->eventInputQueue.getFirst();
}

bool NetworkManager::inputEventAvailable()
{
    return !this->eventInputQueue.empty();
}

void NetworkManager::unsetRegistry()
{
    this->registry = nullptr;
}

node::Contact* NetworkManager::getNodeAddress()
{
    return this->node->getContactManager()->getOwnContact();
}

std::vector<node::Contact*> NetworkManager::getLeafs()
{
    return this->leafs;
}

void NetworkManager::setLeafs(std::vector<node::Contact*> leafs)
{
    this->leafs = leafs;
}

common::NodeType NetworkManager::getNodeType()
{
    return this->node->getNodeType();
}

void NetworkManager::setBroadcastSocket(int bcSocket)
{
    this->broadcastSocket = bcSocket;
}

std::string NetworkManager::getBroadCastAddress()
{
    return this->broadCastAddress;
}

std::string NetworkManager::getBroadcastPort()
{
    return this->broadcastPort;
}

node::Contact* NetworkManager::getRegistry()
{
    return this->registry;
}

std::string& NetworkManager::getLastASPModel()
{
    return this->node->getLastASPNetworkModel();
}
} // namespace network
} // namespace knowledge_registry
