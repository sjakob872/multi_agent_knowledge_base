#include "knowledge_registry/node/Node.h"
#include <csignal>
#include <memory>

int main(int argc, char **argv)

{
    auto* node = new knowledge_registry::node::Node();

    signal(SIGINT, knowledge_registry::node::Node::nodeSigintHandler);

    if(argc > 1) {
        if(std::string(argv[1]) == "--eureka") {
            node->setInteractWithEureka(true);
        }
    }

    node->init();

    while(node->isRunning())
    {
        sleep(1);
    }

    node->shutdown();
    delete node;
    return 0;
}
