#include "knowledge_registry/common/JsonSerializer.h"

#include <jsoncpp/json/reader.h>
#include <jsoncpp/json/writer.h>

namespace knowledge_registry
{
namespace common
{
void JsonSerializer::Serialize(IJsonSerializable& obj, std::string& output)
{
    Json::Value root;
    obj.Serialize(root);
    Json::StyledWriter writer;
    output = writer.write(root);
}

bool JsonSerializer::Deserialize(IJsonSerializable* obj, std::string& input)
{
    if (obj == nullptr) {
        return false;
    }
    Json::Value root;
    Json::Reader reader;
    if (!reader.parse(input, root)) {
        return false;
    }
    obj->Deserialize(root);
    return true;
}
} // namespace common
} // namespace knowledge_registry