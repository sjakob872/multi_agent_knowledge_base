# README #

This repository presents the work in progress of the multi-agent based knowledge registry. 

### How do I get set up? ###
In order to compile the Code, a Linux system (tested on Ubuntu 18.04) is needed.
The installation is started by running the initialize_workspace.sh script. Please create a folder named **workspace** in your home folder, copy the script into this folder, and make it executable. Afterwards, run the script in the **workspace** folder.
It will install [ROS](https://www.ros.org/) and neccessary dependencies. After the successfull exectuion of the script, navigate to the folder **src/multi_agent_knowldge_base**. The repository can be build by running **catkin build --this**. To enable ROS to find the package and the executable, run **source ~/.bashrc**.

To start a node of the multi-agent knowledge base, execute **rosrun knowledge_registry knowledge_registry**. For each new node, new terminal has to be opened and the command **rosrun knowledge_registry knowledge_registry** has to be called. The nodes wil elect single registry and the remaining nodes will act as leafs.
