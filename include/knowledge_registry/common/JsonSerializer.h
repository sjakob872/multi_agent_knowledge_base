#pragma once

#include "knowledge_registry/common/IJsonSerializable.h"

namespace knowledge_registry
{
namespace common
{
class JsonSerializer
{
public:
    static void Serialize(IJsonSerializable& obj, std::string& output);
    static bool Deserialize(IJsonSerializable* obj, std::string& input);

private:
    JsonSerializer() = default;
};
} // namespace common
} // namespace knowledge_registry