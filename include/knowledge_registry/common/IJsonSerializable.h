#pragma once

#include <jsoncpp/json/value.h>

namespace knowledge_registry
{
namespace common
{
class IJsonSerializable
{
public:
    virtual ~IJsonSerializable() = default;

    virtual void Serialize(Json::Value& root) = 0;
    virtual void Deserialize(Json::Value& root) = 0;
};
} // namespace common
} // namespace knowledge_registry
