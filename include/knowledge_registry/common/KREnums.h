#pragma once

namespace knowledge_registry
{
namespace common
{

#define UNUSED_VAR __attribute__((unused))
// Strings
#define MAKE_STRINGS(VAR) #VAR,

// Enum
#define MAKE_ENUM(VAR) VAR,

#define MESSAGE_TYPE(MSG)                                                                                                                                      \
    MSG(UNDEFINED_MSG_TYPE)                                                                                                                                    \
    MSG(BROADCAST)                                                                                                                                             \
    MSG(TCP)                                                                                                                                                   \
    MSG(ASP)                                                                                                                                                   \
    MSG(HTTP_REST)

enum MessageType
{
    MESSAGE_TYPE(MAKE_ENUM)
};

UNUSED_VAR static const char* messageTypes[] = {MESSAGE_TYPE(MAKE_STRINGS)};

#define PARSER_TYPE(PARSER)                                                                                                                                    \
    PARSER(IP_PORT)                                                                                                                                            \
    PARSER(NODE)                                                                                                                                               \
    PARSER(REG)

enum ParserType
{
    PARSER_TYPE(MAKE_ENUM)
};

UNUSED_VAR static const char* parserTypes[] = {PARSER_TYPE(MAKE_STRINGS)};

#define NODE_TYPE(NODE)                                                                                                                                        \
    NODE(DEFAULT)                                                                                                                                              \
    NODE(REGISTRY)                                                                                                                                             \
    NODE(LEAF)

enum NodeType
{
    NODE_TYPE(MAKE_ENUM)
};

UNUSED_VAR static const char* nodeTypes[] = {NODE_TYPE(MAKE_STRINGS)};

#define MESSAGE_ROLE(ROLE)                                                                                                                                     \
    ROLE(UNDEFINED_MSG_ROLE)                                                                                                                                   \
    ROLE(REGISTRY_REQUEST)                                                                                                                                     \
    ROLE(ASP_NETWORK_REQUEST)                                                                                                                                  \
    ROLE(ASP_PUT)                                                                                                                                              \
    ROLE(ASP_GET)                                                                                                                                              \
    ROLE(POST)                                                                                                                                                 \
    ROLE(REGISTER)                                                                                                                                             \
    ROLE(HTTPREGISTER)                                                                                                                                         \
    ROLE(HTTPDEREGISTER)                                                                                                                                       \
    ROLE(RESPONSE)                                                                                                                                             \
    ROLE(FAILURE)                                                                                                                                              \
    ROLE(ACKNOWLEDGE)                                                                                                                                          \
    ROLE(PING)                                                                                                                                                 \
    ROLE(ELECTION)

enum MessageRole
{
    MESSAGE_ROLE(MAKE_ENUM)
};

UNUSED_VAR static const char* messageRoles[] = {MESSAGE_ROLE(MAKE_STRINGS)};

#define SOLVER_TYPE(TYPE)                                                                                                                                      \
    TYPE(NETWORK)                                                                                                                                              \
    TYPE(KNOWLEDGE_BASE)                                                                                                                                       \
    TYPE(KNOWLEDGE_BASE_DEBUG)

enum SolverType
{
    SOLVER_TYPE(MAKE_ENUM)
};

UNUSED_VAR static const char* solverTypes[] = {SOLVER_TYPE(MAKE_STRINGS)};

} // namespace common
} // namespace knowledge_registry
