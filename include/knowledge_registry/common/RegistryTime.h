#pragma once

#include <cstdint>
#include <ostream>

namespace knowledge_registry
{
namespace common
{
class RegistryTime
{
public:
    constexpr RegistryTime()
            : _time(0LL)
    {
    }

    constexpr int64_t inNanoseconds() const { return _time; }
    constexpr int64_t inMicroseconds() const { return _time / 1000LL; }
    constexpr int64_t inMilliseconds() const { return _time / 1000000LL; }
    constexpr int64_t inSeconds() const { return _time / 1000000000LL; }
    constexpr int64_t inMinutes() const { return _time / (1000000000LL * 60LL); }
    constexpr int64_t inHours() const { return _time / (1000000000LL * 60LL * 60LL); }

    static constexpr RegistryTime zero() { return RegistryTime(); }

    template <typename T>
    static constexpr RegistryTime nanoseconds(T n)
    {
        return RegistryTime(n);
    }

    template <typename T>
    static constexpr RegistryTime microseconds(T n)
    {
        return RegistryTime(1000LL * n);
    }

    template <typename T>
    static constexpr RegistryTime milliseconds(T n)
    {
        return RegistryTime(1000000LL * n);
    }

    template <typename T>
    static constexpr RegistryTime seconds(T n)
    {
        return RegistryTime(1000000000LL * n);
    }

    template <typename T>
    static constexpr RegistryTime minutes(T n)
    {
        return RegistryTime(1000000000LL * 60LL * n);
    }

    template <typename T>
    static constexpr RegistryTime hours(T n)
    {
        return RegistryTime(1000000000LL * 60LL * 60LL * n);
    }

    constexpr RegistryTime operator+(const RegistryTime& t) const { return RegistryTime(_time + t.inNanoseconds()); }

    constexpr RegistryTime operator-(const RegistryTime& t) const { return RegistryTime(_time - t.inNanoseconds()); }

    RegistryTime& operator-=(const RegistryTime& t)
    {
        _time -= t.inNanoseconds();
        return *this;
    }

    RegistryTime& operator+=(const RegistryTime& t)
    {
        _time += t.inNanoseconds();
        return *this;
    }

    template <typename T>
    constexpr RegistryTime operator/(T t) const
    {
        return RegistryTime(_time / t);
    }

    template <typename T>
    constexpr RegistryTime operator*(T t) const
    {
        return RegistryTime(_time * t);
    }

    template <typename T>
    RegistryTime& operator/=(T t)
    {
        _time /= t;
        return *this;
    }

    template <typename T>
    RegistryTime& operator*=(T t)
    {
        _time *= t;
        return *this;
    }

    constexpr bool operator<(const RegistryTime& t) const { return _time < t.inNanoseconds(); }

    constexpr bool operator<=(const RegistryTime& t) const { return _time <= t.inNanoseconds(); }

    constexpr bool operator>(const RegistryTime& t) const { return _time > t.inNanoseconds(); }

    constexpr bool operator>=(const RegistryTime& t) const { return _time >= t.inNanoseconds(); }

    constexpr bool operator==(const RegistryTime& t) const { return _time == t.inNanoseconds(); }

    constexpr bool operator!=(const RegistryTime& t) const { return _time != t.inNanoseconds(); }

    friend std::ostream& operator<<(std::ostream& os, const RegistryTime& time);

private:
    template <typename T>
    constexpr explicit RegistryTime(T t)
            : _time(t)
    {
    }

    int64_t _time;
};

} // namespace common
} // namespace knowledge_registry
