#pragma once

#include <map>

namespace knowledge_registry
{
namespace node
{
class Node;
class Contact;

class ContactManager
{
    friend class Node;

  public:
    explicit ContactManager(Node *node);
    virtual ~ContactManager();
    Contact *getContact(std::string ip, std::string port, std::string uuid, std::string topic = "UNDEFINED");
    Contact *getOwnContact();

  private:
    Node *node;
    Contact *ownContact;
    std::map<std::string, Contact *> contacts;
};

} // namespace node
} // namespace knowledge_registry
