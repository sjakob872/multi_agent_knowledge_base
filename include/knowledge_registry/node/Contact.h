#pragma once

#include <string>

namespace knowledge_registry
{
namespace node
{
class Contact
{
    friend class ContactManager;

  public:
    const std::string &getIP();
    const std::string &getPort();
    const std::string &getUuid();
    const std::string &getTopic();

    std::string toString();
  private:
    Contact(std::string ip, std::string port, std::string uuid, std::string topic = "UNDEFINED");

    std::string ip;
    std::string port;
    std::string uuid;
    std::string topic;
};
} // namespace node
} // namespace knowledge_registry