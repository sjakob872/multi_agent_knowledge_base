#pragma once

#include "knowledge_registry/node/Contact.h"
#include "knowledge_registry/common/KREnums.h"

#include <memory>
#include <vector>

namespace knowledge_registry
{
namespace common
{
template <typename>
class InformationElement;
}
namespace node
{
class InEvent;
class Node;
class ASPParser
{
public:
    static std::vector<Contact*> parse(Node* node, std::string aspString, common::ParserType parserType);
    static std::string generateASPContact(std::shared_ptr<const common::InformationElement<node::InEvent>> message, bool registry);
    static std::string getASPRepresentation(Node* node, std::shared_ptr<const common::InformationElement<node::InEvent>> message, bool root);
    static std::string getExternal(Contact* contact, bool registry);

private:
    static std::vector<Contact*> extractContacts(Node* node, std::string aspString, std::string regularExp);
};
} // namespace node
} // namespace knowledge_registry