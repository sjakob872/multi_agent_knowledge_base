#pragma once

#include <condition_variable>
#include <map>
#include <mutex>
#include <thread>

namespace knowledge_registry
{
namespace node
{
class Contact;
class Node;
class PingManager
{
public:
    explicit PingManager(Node* node);
    ~PingManager();

    void pingReceivedFromLeaf(Contact* contact);
    void notify();

private:
    // Start ping or get ping to know if the registry are still alive:
    void startPing();
    void pingLeafs();
    void getPing();
    bool pingLeaf(Contact* contact);

    Node* node;

    std::condition_variable sleepCV;
    std::mutex sleepMutex;
    bool sleepFlag;

    std::mutex pingMutex;
    std::condition_variable pingCV;
    std::map<Contact*, int> pingedLeafs;
    static const int pingLimit;
    std::thread* startPingThread;
};
} // namespace node
} // namespace knowledge_registry
