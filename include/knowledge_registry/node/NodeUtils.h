#pragma once

#include "knowledge_registry/common/KREnums.h"

#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace reasoner
{
namespace asp
{
class Solver;
}
} // namespace reasoner
namespace knowledge_registry
{
namespace common
{
template <typename>
class InformationElement;
}
namespace node
{
class Contact;
class Node;
class InEvent;
class NodeUtils
{
public:
    explicit NodeUtils(Node* node);
    virtual ~NodeUtils() = default;

    std::string getNetworkSolution(
            std::shared_ptr<const common::InformationElement<InEvent>> event, std::pair<common::SolverType, reasoner::asp::Solver*>* solverPair);
    std::string getSolution(
            std::shared_ptr<const common::InformationElement<InEvent>> event, std::pair<common::SolverType, reasoner::asp::Solver*>* solverPair);
    std::vector<Contact*> getLeafs();
    std::vector<std::string> getFittingPredicates(std::string aspQuery, std::pair<common::SolverType, reasoner::asp::Solver*>* solverPair);

private:
    friend class Node;
    Node* node;
    unsigned long counter;

    std::string setLasModel(std::pair<common::SolverType, reasoner::asp::Solver*>* solverPair) const;
};
} // namespace node
} // namespace knowledge_registry