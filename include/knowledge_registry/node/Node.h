#pragma once

#include "knowledge_registry/common/KREnums.h"

#include <cantcoap/cantcoap.h>

#include <condition_variable>
#include <ifaddrs.h>
#include <iostream>
#include <mutex>
#include <queue>
#include <thread>

namespace reasoner
{
namespace asp
{
class Solver;
}
} // namespace reasoner

namespace knowledge_registry
{
namespace common
{
template <typename>
class InformationElement;
}
namespace network
{
class NetworkManager;
class Handler;
} // namespace network
namespace node
{
class PingManager;
class SolverManager;
class NodeUtils;
class ContactManager;
class Contact;
class InEvent;
class Node
{

public:
    Node() = default;
    virtual ~Node();

    ContactManager* getContactManager() const;
    SolverManager* getSolverManager() const;
    network::NetworkManager* getNetworkManager() const;
    Contact* getRegistry() const;
    common::NodeType getNodeType();

    char* getIP();
    char* getNetmask();
    std::string getFreePort();
    std::string getPort();
    std::string getUuid();
    std::string getTopic() const;
    void setTopic(std::string topic);
    void setLastASPNetworkModel(std::string solution);
    void setLastASPKnowledgeBaseModel(std::string solution);
    std::string& getLastASPNetworkModel();
    std::string& getLastASPKnowledgeBaseModel();

    void notify();
    bool isRunning();
    bool isStay() const;
    static void nodeSigintHandler(int sig);

    void setInteractWithEureka(bool interactWithEureka);
    void init();

    void shutdown();
    static const std::string UNDEFINED_TOPIC;

    static bool running;

    bool isInteractWithEureka() const;

protected:
    friend class PingManager;
    SolverManager* solverManager;
    network::NetworkManager* networkManager;
    ContactManager* contactManager;
    PingManager* pingManager;

    NodeUtils* nodeUtils;

    Contact* registry;
    std::string topic;
    std::string port;
    char* ip;
    char* netmask;
    common::NodeType nodeType;
    std::string uuid;

    std::string lastASPNetworkModel;
    std::string lastASPKnowledgeBaseModel;

    std::vector<std::shared_ptr<const common::InformationElement<node::InEvent>>> broadcastMessageList;
    bool stay;

    bool registryAlive;
    bool interactWithEureka;

    bool ignoreOwnBroadcasts;
    int countBMessage;

    std::thread* startThread;

    std::thread* nodeEventThread;

    std::mutex newEventMutex;
    std::condition_variable newEventCV;

    // Check RegistryMessage
    virtual void eventReceived();

    int startNode();
    void setUuid();
    void getOwnNetworkConfiguration();
    void extractInterfaceInformation(ifaddrs* temp_addr, const std::string& interface);

    std::string startElection();
    void setRegistryAlive(bool alive);
    void setRegistry(std::string ip, std::string port, std::string uuid, std::string topic);
    void unsetRegistry();

    void forwardEventToLeafs(std::shared_ptr<const common::InformationElement<InEvent>> event);

    std::string buildSolutionString(std::shared_ptr<const common::InformationElement<node::InEvent>> currentInputEvent,
            std::pair<common::SolverType, reasoner::asp::Solver*>* solverPair);
};
} // namespace node
} // namespace knowledge_registry