#pragma once

#include "knowledge_registry/node/event/Event.h"

namespace knowledge_registry
{
namespace node
{
class InEvent : public Event
{
public:
    InEvent(common::MessageType type, common::MessageRole role, std::string ip, std::string port, std::string uuid, std::string message = "");
};
} // namespace node
} // namespace knowledge_registry
