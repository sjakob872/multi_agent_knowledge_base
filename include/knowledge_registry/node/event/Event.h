#pragma once

#include "knowledge_registry/common/KREnums.h"
#include "knowledge_registry/common/IJsonSerializable.h"

namespace knowledge_registry
{
namespace node
{
class Event : public common::IJsonSerializable
{
public:
    Event(common::MessageType type, common::MessageRole role, std::string ip, std::string port, std::string uuid, std::string message = "");
    virtual ~Event() = default;

    std::string getIP() const;
    std::string getMessage() const;
    common::MessageRole getRole() const;
    common::MessageType getType() const;
    std::string getPort() const;
    std::string getUuid() const;

    std::string toString();

    void Serialize(Json::Value& root) override ;
    void Deserialize(Json::Value& root) override;


protected:
    common::MessageType type;
    common::MessageRole role;

    std::string ip;
    std::string port;
    std::string uuid;
    std::string message;
};
} // namespace node
} // namespace knowledge_registry