#pragma once

#include "knowledge_registry/common/KREnums.h"

#include <utility>

namespace reasoner
{
namespace asp
{
class Solver;
}
} // namespace reasoner

namespace knowledge_registry
{
namespace node
{
class Node;
class SolverManager
{
public:
    explicit SolverManager(Node* node);
    ~SolverManager();
    std::pair<common::SolverType, reasoner::asp::Solver*>* getNetworkSolverPair() const;
    std::pair<common::SolverType, reasoner::asp::Solver*>* getKnowledgeBaseSolverPair() const;
    std::pair<common::SolverType, reasoner::asp::Solver*>* getKnowledgeBaseDebugSolverPair() const;

    void initSolvers();

private:
    std::pair<common::SolverType, reasoner::asp::Solver*>* networkSolverPair;
    std::pair<common::SolverType, reasoner::asp::Solver*>* knowledgeBaseSolverPair;
    std::pair<common::SolverType, reasoner::asp::Solver*>* knowledgeBaseDebugSolverPair;
    Node* node;
};
} // namespace node
} // namespace knowledge_registry
