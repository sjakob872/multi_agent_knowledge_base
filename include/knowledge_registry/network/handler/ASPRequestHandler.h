#pragma once

#include "knowledge_registry/network/handler/Handler.h"

namespace knowledge_registry
{
namespace node
{
class OutEvent;
} // namespace node
namespace network
{
class NetworkManager;
class ASPRequestHandler : public Handler
{

public:
    explicit ASPRequestHandler(NetworkManager* networkManager);

    bool isResponsible(std::shared_ptr<const common::InformationElement<node::OutEvent>> event) override;
    void handleEvent(std::shared_ptr<const common::InformationElement<node::OutEvent>> event) override;
};
} // namespace network
} // namespace knowledge_registry
