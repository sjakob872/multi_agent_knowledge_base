#pragma once

#include "knowledge_registry/network/handler/Handler.h"

namespace knowledge_registry
{
namespace node
{
class OutEvent;
} // namespace node
namespace network
{
class NetworkManager;
class BroadcastHandler : public Handler
{

public:
    explicit BroadcastHandler(NetworkManager* networkManager);
    ~BroadcastHandler() override = default;

    void handleEvent(std::shared_ptr<const common::InformationElement<node::OutEvent>> event) override;
    bool isResponsible(std::shared_ptr<const common::InformationElement<node::OutEvent>> event) override;

private:
    void sendBroadcast(std::shared_ptr<const common::InformationElement<node::OutEvent>> event);
    int prepareSocket();

    int broadcastSocketFD;
};

} // namespace network
} // namespace knowledge_registry
