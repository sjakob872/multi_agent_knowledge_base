#pragma once

#include <cantcoap/cantcoap.h>
#include <memory>

namespace knowledge_registry
{
namespace common
{
template <typename>
class InformationElement;
}
namespace node
{
class OutEvent;
} // namespace node
namespace network
{
class NetworkManager;
class Handler
{
public:
    explicit Handler(NetworkManager* networkManager);
    virtual ~Handler() = default;

    virtual bool isResponsible(std::shared_ptr<const common::InformationElement<node::OutEvent>> nodeEvent) = 0;
    virtual void handleEvent(std::shared_ptr<const common::InformationElement<node::OutEvent>> nodeEvent) = 0;

protected:
    void sendMessage(const std::string& sendPort, const std::string& remoteIP, const std::string& remotePort, std::shared_ptr<const common::InformationElement<node::OutEvent>> event);
    NetworkManager* networkManager;
    static CoapPDU* generatePDU(std::shared_ptr<const common::InformationElement<node::OutEvent>> event, bool confirm);
};
} // namespace network
} // namespace knowledge_registry
