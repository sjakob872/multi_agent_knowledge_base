#pragma once

#include "knowledge_registry/network/handler/Handler.h"

#include <curl/curl.h>

namespace knowledge_registry
{
namespace node
{
class OutEvent;
} // namespace node
namespace network
{
class NetworkManager;
class HTTPRestHandler : public Handler
{
public:
    explicit HTTPRestHandler(NetworkManager* networkManager);
    ~HTTPRestHandler() override = default;

    void handleEvent(std::shared_ptr<const common::InformationElement<node::OutEvent>> event) override;
    bool isResponsible(std::shared_ptr<const common::InformationElement<node::OutEvent>> event) override;

private:
    void registerAtEurekaServer(std::shared_ptr<const common::InformationElement<node::OutEvent>> event);
    void deregisterAtEurekaServer(std::shared_ptr<const common::InformationElement<node::OutEvent>> event);
    static std::size_t callback(const char* in, std::size_t size, std::size_t num, std::string* out);

    void httpPost(const std::string& url, const std::string& request);
    void httpDelete(const std::string& url);
    std::string httpGet(const std::string& url);

    void* generateCurlRequest(const std::string& url, curl_slist* headers, std::string* httpData) const;
};
} // namespace network
} // namespace knowledge_registry