#pragma once

#include "knowledge_registry/common/InfoBuffer.h"
#include "knowledge_registry/common/InformationElement.h"
#include "knowledge_registry/common/KREnums.h"

#include <cantcoap/cantcoap.h>
#include <condition_variable>
#include <thread>
#include <vector>

namespace knowledge_registry
{
namespace node
{
class Contact;
class Node;
class OutEvent;
class InEvent;
} // namespace node
namespace network
{
class Handler;
class BroadcastReceiver;
class TCPReceiver;
class NetworkManager
{

public:
    //    Constructor
    explicit NetworkManager(node::Node* node);
    ~NetworkManager();

    common::NodeType getNodeType();
    std::string calculateBroadcastAddress();

    //  Add and get Queue.
    void addToInputQueue(std::shared_ptr<common::InformationElement<node::InEvent>> event);
    void addToOutputQueue(std::shared_ptr<common::InformationElement<node::OutEvent>> event);

    std::shared_ptr<const common::InformationElement<node::InEvent>> getCurrentInputEvent();
    bool inputEventAvailable();

    // Add ang get broadcastAddress, broadcastSocket.
    void setBroadcastSocket(int broadcastSocket);
    std::string getBroadcastPort();
    std::string getBroadCastAddress();

    // Check where ist for the nodeEvent responsible.
    void checkResponsible();
    void checkInputQueue();

    node::Contact* getRegistry();
    void unsetRegistry();

    // Node information
    std::string getFreePort();
    node::Contact* getNodeAddress();
    std::vector<node::Contact*> getLeafs();
    void setLeafs(std::vector<node::Contact*> leafs);

    std::string& getLastASPModel();

protected:
    friend class node::Node;
    node::Node* node;

    BroadcastReceiver* broadcastReceiver;
    TCPReceiver* tcpReceiver;
    std::vector<Handler*> handlers;

    // Broadcast variables
    std::string broadCastAddress;
    int broadcastSocket;
    std::string broadcastPort;

    common::InfoBuffer<node::OutEvent> eventOutputQueue;
    common::InfoBuffer<node::InEvent> eventInputQueue;

    // Node information
    node::Contact* registry;
    std::vector<node::Contact*> leafs;

    std::thread* checkResponsibleThread;
    std::thread* checkInputQueueThread;

    std::mutex outputQueueMutex;
    std::mutex inputQueueMutex;

    std::condition_variable outputQueueCV;
    std::condition_variable inputQueueCV;
    bool stay;
    static const int queueSize = 25;
};
} // namespace network
} // namespace knowledge_registry