#pragma once

#include "knowledge_registry/network/receiver/Receiver.h"

namespace knowledge_registry
{
namespace network
{
class NetworkManager;

class BroadcastReceiver : public Receiver
{
public:
    explicit BroadcastReceiver(NetworkManager* networkManager);
    virtual ~BroadcastReceiver();

    int receiveBroadcast();

private:
    std::thread* receiveBroadcastThread;
    int broadcastSocketFD;
};
} // namespace network
} // namespace knowledge_registry