#pragma once

#include "knowledge_registry/network/receiver/Receiver.h"

namespace knowledge_registry
{
namespace network
{
class NetworkManager;

class TCPReceiver : public Receiver
{
public:
    explicit TCPReceiver(NetworkManager* networkManager);
    ~TCPReceiver();

    int receiveTCP();

private:
    std::thread* receiveIpThread;
    int socketFD;
};
} // namespace network
} // namespace knowledge_registry