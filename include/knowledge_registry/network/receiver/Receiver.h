#pragma once

#include <cantcoap/cantcoap.h>

#include <thread>

namespace knowledge_registry
{
namespace network
{
class NetworkManager;
class Receiver
{
public:
    explicit Receiver(NetworkManager* networkManager);
    ~Receiver() = default;

    int receive(int socketFD, const std::string& receiverName);

// buffers for UDP and URIs
#define BUF_LEN 500
    char buffer[BUF_LEN];

protected:
    NetworkManager* networkManager;
    bool stay;

    bool haveInput(int fd, double timeout);
    bool flushSocketBeforeClose(int fd, double timeout);
    int getSO_ERROR(int fd);
    void closeSocket(int fd);
};
} // namespace network
} // namespace knowledge_registry