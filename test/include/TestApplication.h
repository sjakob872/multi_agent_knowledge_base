#pragma once

#include "knowledge_registry/node/Node.h"
#include <thread>

class TestApplication : public knowledge_registry::node::Node
{

public:
    TestApplication();
    ~TestApplication();
    void eventReceived() override;
private:
    std::thread* inputThread;
    int receiveConsoleInput();
};
