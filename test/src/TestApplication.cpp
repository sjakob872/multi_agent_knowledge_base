#include "TestApplication.h"

#include "knowledge_registry/common/InformationElement.h"
#include "knowledge_registry/common/KREnums.h"
#include "knowledge_registry/network/NetworkManager.h"
#include "knowledge_registry/node/Contact.h"
#include "knowledge_registry/node/NodeUtils.h"
#include "knowledge_registry/node/SolverManager.h"
#include "knowledge_registry/node/event/InEvent.h"
#include "knowledge_registry/node/event/OutEvent.h"

#include <reasoner/asp/Solver.h>

#include <uuid/uuid.h>

#define BUF_LEN 500

TestApplication::TestApplication()
{
    this->stay = true;
    this->inputThread = new std::thread(&TestApplication::receiveConsoleInput, this);
}

TestApplication::~TestApplication()
{
    this->stay = false;
    this->inputThread->join();
    delete this->inputThread;
}

void TestApplication::eventReceived()
{
    while (this->stay) {

        std::unique_lock<std::mutex> lk(this->newEventMutex);

        std::cout << "TestApplication: waiting for incoming messages" << std::endl;
        this->newEventCV.wait(lk);

        lk.unlock();

        while (this->networkManager->inputEventAvailable()) {
            auto currentMessage = this->networkManager->getCurrentInputEvent();
            std::cout << "TestApplication: Received message: " << currentMessage->getInformation().toString() << std::endl;
            std::cout << "TestApplication: type: " << knowledge_registry::common::nodeTypes[this->nodeType] << ". Is registry set ? "
                      << (!this->registry ? "no" : "yes") << std::endl;
            switch (currentMessage->getInformation().getType()) {
            case knowledge_registry::common::MessageType::TCP:
                if (currentMessage->getInformation().getRole() == knowledge_registry::common::MessageRole::POST) {
                    this->registryAlive = true;
                    this->setRegistry(currentMessage->getInformation().getIP(), currentMessage->getInformation().getPort(),
                            currentMessage->getInformation().getUuid(), "");
                    std::string solution = this->nodeUtils->getNetworkSolution(currentMessage,
                                                                               this->solverManager->getNetworkSolverPair());
                    this->networkManager->addToOutputQueue(std::make_shared<knowledge_registry::common::InformationElement<knowledge_registry::node::OutEvent>>(
                            knowledge_registry::node::OutEvent(knowledge_registry::common::MessageType::ASP,
                                    knowledge_registry::common::MessageRole::ASP_NETWORK_REQUEST, currentMessage->getInformation().getIP(),
                                    currentMessage->getInformation().getPort(), currentMessage->getInformation().getUuid(),
                                    "leaf(" + this->solverManager->getNetworkSolverPair()->second->WILDCARD_STRING + ", " +
                                            this->solverManager->getNetworkSolverPair()->second->WILDCARD_STRING + ", " +
                                            this->solverManager->getNetworkSolverPair()->second->WILDCARD_STRING + ")")));
                    std::cout << "TestApplication: Sending ASP Request" << std::endl;
                } else {
                    std::cout << "TestApplication: no registry information" << std::endl;
                }

                break;

            case knowledge_registry::common::MessageType::ASP:
                if (currentMessage->getInformation().getRole() == knowledge_registry::common::MessageRole::RESPONSE) {
                    std::cout << "\033[1;32mTestApplication: Receive response from: " << currentMessage->getInformation().toString() << "\033[0m" << std::endl;
                } else {
                    std::cout << "\033[1;31mTestApplication: not supported message: " << currentMessage->getInformation().toString() << "\033[0m" << std::endl;
                }
                break;

            default:
                break;
            }
        }
    }
}

int TestApplication::receiveConsoleInput()
{
    std::string delimiter = " ";
    std::string line;
    while (this->stay) {
        getline(std::cin, line);
        size_t pos = line.find(delimiter);
        std::string command = line.substr(0, pos);
        std::string aspString = line.substr(pos + 1);
        if (command == knowledge_registry::common::messageRoles[knowledge_registry::common::MessageRole::ASP_GET]) {
            this->networkManager->addToOutputQueue(std::make_shared<knowledge_registry::common::InformationElement<knowledge_registry::node::OutEvent>>(
                    knowledge_registry::node::OutEvent(knowledge_registry::common::MessageType::ASP, knowledge_registry::common::MessageRole::ASP_GET,
                            this->registry->getIP(), this->registry->getPort(), this->registry->getUuid(), aspString)));
        } else if (command == knowledge_registry::common::messageRoles[knowledge_registry::common::MessageRole::ASP_PUT]) {
            this->networkManager->addToOutputQueue(std::make_shared<knowledge_registry::common::InformationElement<knowledge_registry::node::OutEvent>>(
                    knowledge_registry::node::OutEvent(knowledge_registry::common::MessageType::ASP, knowledge_registry::common::MessageRole::ASP_PUT,
                            this->registry->getIP(), this->registry->getPort(), this->registry->getUuid(), aspString)));
        } else if (command.empty()) {
            continue;
        } else {
            std::cout << "\033[1;31mTestApplication: command: " << command
                      << " not supported. Please use: " << knowledge_registry::common::messageRoles[knowledge_registry::common::MessageRole::ASP_GET] << " or "
                      << knowledge_registry::common::messageRoles[knowledge_registry::common::MessageRole::ASP_PUT] << "\033[0m" << std::endl;
            continue;
        }
    }
    return 0;
}
