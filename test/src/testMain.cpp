#include "TestApplication.h"
#include <csignal>
#include <memory>

int main(int argc, char **argv)
{

    std::cout << "\033[1;31m#########################################################################\033[0m" << std::endl;
    std::cout << "\033[1;31m###### Press Ctrl+C and then Ctrl+D to terminate the application ########\033[0m" << std::endl;
    std::cout << "\033[1;31m#########################################################################\033[0m" << std::endl;
    // Start Node
    auto* application = new TestApplication();

    signal(SIGINT, TestApplication::nodeSigintHandler);

    if(argc > 1) {
        if(std::string(argv[1]) == "--eureka") {
            application->setInteractWithEureka(true);
        }
    }

    application->init();

    while(application->isRunning())
    {
        sleep(1);
    }

    application->shutdown();
    delete application;
    return 0;
}
